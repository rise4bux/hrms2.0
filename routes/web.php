<?php

use App\Http\Controllers\Backend\DepartmentController;
use App\Http\Controllers\Backend\ModuleMasterController;
use App\Models\ModuleMaster;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Backend\TagController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/dashboard', function () {
    if(auth()->user()->hasRole('admin')){
        return redirect()->route('role.index');
    }else{
//        return view('backend.employee.view');
    }
})->middleware('auth')->name('dashboard');

require __DIR__ . '/auth.php';

Route::group(['middleware'=>['auth']],function (){

    Route::group(['namespace' => 'Auth'], function () {
        Route::get('view-profile', [\App\Http\Controllers\Auth\ProfileController::class,'showProfile'])->name('view.profile');
        Route::get('update-profile', [\App\Http\Controllers\Auth\ProfileController::class,'editProfile'])->name('edit.profile');
        Route::post('update-profile', [\App\Http\Controllers\Auth\ProfileController::class,'updateProfile'])->name('update.profile');

        Route::get('change-password', [\App\Http\Controllers\Auth\ProfileController::class,'changePassword'])->name('change.password');
        Route::post('change-password', [\App\Http\Controllers\Auth\ProfileController::class,'updatePassword'])->name('update.password');
        Route::get('change-email', [\App\Http\Controllers\Auth\ProfileController::class,'changeOwnerEmail'])->name('email.create');
        Route::get('update-email', [\App\Http\Controllers\Auth\ProfileController::class,'updateOwnerEmail'])->name('update.email');
    });

    Route::group(['namespace' => 'Backend'], function () {

        Route::group(['prefix'=>'role-management/roles','middleware'=>'role:admin'],function (){
            Route::get('/',[\App\Http\Controllers\Backend\RoleController::class,'index'])->name('role.index');
            Route::get('/create/{id?}',[\App\Http\Controllers\Backend\RoleController::class,'create'])->name('role.create');
            Route::post('/store/{id?}',[\App\Http\Controllers\Backend\RoleController::class,'store'])->name('role.store');
            Route::get('/delete/{id}',[\App\Http\Controllers\Backend\RoleController::class,'delete'])->name('role.delete');
        });

        Route::group(['prefix'=>'role-management/permission','middleware' => 'role:admin'],function (){
            Route::get('/{role?}',[\App\Http\Controllers\Backend\PermissionController::class,'index'])->name('permission.index');
            Route::get('/create',[\App\Http\Controllers\Backend\PermissionController::class,'create'])->name('permission.create');
            Route::post('/store',[\App\Http\Controllers\Backend\PermissionController::class,'store'])->name('permission.store');
            Route::post('/save-treeview',[\App\Http\Controllers\Backend\PermissionController::class,'saveTreeview'])->name('permission.save-treeview');
        });

        Route::group(['prefix'=>'user','middleware' => 'role:admin'],function (){
            Route::get('/',[\App\Http\Controllers\Backend\UserController::class,'index'])->name('user.index');
            Route::get('/create',[\App\Http\Controllers\Backend\UserController::class,'create'])->name('user.create');
            Route::post('/store',[\App\Http\Controllers\Backend\UserController::class,'store'])->name('user.store');
        });

        Route::group(['prefix'=>'role-permission','middleware' => 'role:admin'],function (){
            Route::get('/',[\App\Http\Controllers\Backend\RolePermissionController::class,'index'])->name('rolepermission.index');
            Route::get('/create',[\App\Http\Controllers\Backend\RolePermissionController::class,'create'])->name('rolepermission.create');
            Route::post('/store',[\App\Http\Controllers\Backend\RolePermissionController::class,'store'])->name('rolepermission.store');
        });

        Route::group(['prefix'=>'role-management/module-master','middleware' => 'role:admin'],function (){
            Route::get('/',[\App\Http\Controllers\Backend\ModuleMasterController::class,'index'])->name('modulemaster.index');
            Route::get('/create/{id?}',[\App\Http\Controllers\Backend\ModuleMasterController::class,'create'])->name('modulemaster.create');
            Route::get('/delete/{id}',[\App\Http\Controllers\Backend\ModuleMasterController::class,'delete'])->name('modulemaster.delete');
            Route::post('/store/{id?}',[\App\Http\Controllers\Backend\ModuleMasterController::class,'store'])->name('modulemaster.store');
            //Route::post('/update',[\App\Http\Controllers\Backend\ModuleMasterController::class,'update'])->name('modulemaster.editedit');
        });

        Route::group(['prefix'=>'employee','middleware' => 'role:admin'],function (){
            Route::get('/',[\App\Http\Controllers\EmployeeController::class,'index'])->name('employee.index');
            Route::get('/create/{id?}',[\App\Http\Controllers\EmployeeController::class,'create'])->name('employee.create')->middleware('role:admin');
            Route::post('/store/{id?}',[\App\Http\Controllers\EmployeeController::class,'store'])->name('employee.store')->middleware('role:admin');
        });

        Route::group(['prefix'=>'clients'],function (){
            Route::get('/',[\App\Http\Controllers\ClientController::class,'index'])->name('client.index')->middleware('hasAnyPermission');
            Route::get('/create',[\App\Http\Controllers\ClientController::class,'create'])->name('client.viewcreate')->middleware('hasPermission:add');
            Route::get('/update/{id?}',[\App\Http\Controllers\ClientController::class,'create'])->name('client.viewupdate')->middleware('hasPermission:update');
            Route::post('/store',[\App\Http\Controllers\ClientController::class,'store'])->name('client.store')->middleware('hasPermission:add');
            Route::post('/update/{id?}',[\App\Http\Controllers\ClientController::class,'store'])->name('client.update')->middleware('hasPermission:update');
            Route::get('/delete/{id}',[\App\Http\Controllers\ClientController::class,'delete'])->name('client.delete')->middleware('hasPermission:delete');
        });

        Route::group(['prefix'=>'projects'],function (){
            Route::get('/',[\App\Http\Controllers\ProjectController::class,'index'])->name('projects.index')->middleware('hasAnyPermission');
            Route::get('/create',[\App\Http\Controllers\ProjectController::class,'create'])->name('projects.viewcreate')->middleware('hasPermission:add');
            Route::get('/update/{id?}',[\App\Http\Controllers\ProjectController::class,'create'])->name('projects.viewupdate')->middleware('hasPermission:update');
            Route::post('/store',[\App\Http\Controllers\ProjectController::class,'store'])->name('projects.store')->middleware('hasPermission:add');
            Route::post('/update/{id?}',[\App\Http\Controllers\ProjectController::class,'store'])->name('projects.update')->middleware('hasPermission:update');
            Route::get('/delete/{id}',[\App\Http\Controllers\ProjectController::class,'delete'])->name('projects.delete')->middleware('hasPermission:delete');
        });

        Route::group(['prefix'=>'role-modules','middleware' => 'role:admin'],function (){
            Route::get('/',[\App\Http\Controllers\RoleModuleController::class,'index'])->name('role-module-controller.index');
        });

    });
    Route::resource('department',DepartmentController::class);
    Route::resource('tags',TagController::class);
});

