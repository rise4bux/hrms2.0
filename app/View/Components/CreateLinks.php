<?php

namespace App\View\Components;

use App\Models\ModuleMaster;
use App\Models\RoleModules;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\View\Component;
use Spatie\Permission\Models\Permission;

class CreateLinks extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $treeData;
    public function __construct($treeData)
    {
        $this->treeData = $treeData;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.create-links');
    }
}
