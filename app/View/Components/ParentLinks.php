<?php

namespace App\View\Components;

use App\Models\ModuleMaster;
use Illuminate\Support\Facades\Log;
use Illuminate\View\Component;
use Spatie\Permission\Models\Permission;

class ParentLinks extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $treeData;
    public $permissionData;
    public function __construct(Permission $permission)
    {
        $this->permissionData = $permission;
        $this->fetchChildrenWithParent(auth()->user()->roles->pluck('id')->first());
    }

    public function fetchChildrenWithParent($role,$id=0){
        try {

            $allParentsData = ModuleMaster::with(['roleModules'=>function($query) use($role){
                $query->where(['role_id' => $role]);
            }])->select('*','name as text')->where('parent_id',$id);
            if($allParentsData->exists()){

                $parentData = $allParentsData->get();
                if($id==0) {
                    $this->treeData = $parentData;
                }

                for ($i=0;$i <count($parentData) ;$i++){
                    $childData = $this->fetchChildrenWithParent($role,$parentData[$i]->id);
                    if(!empty($childData)){
                        if($id==0){
                            $this->treeData[$i]->children = $childData;
                        }else{
                            $parentData[$i]->children = $childData;
                        }
                    }else{
                        if($id == 0){
                            $this->treeData[$i]->children = $this->hasPermissions($this->treeData[$i]->roleModules);
                        }else{
                            $parentData[$i]->children = $this->hasPermissions($parentData[$i]->roleModules);
                        }
                    }
                }
                if($id!=0) {
                    return $parentData;
                }
            }
        }catch (\Exception $e){
            Log::error('Show tree view fetch children with their parent method error : '.$e->getMessage().' line no : '.$e->getLine());
        }
    }

    public function hasPermissions($allModulePermissions){
        try {

            if(isset($allModulePermissions) && !empty($allModulePermissions->first())){
                $allModulePermissions = $allModulePermissions->first()->permissions;
                $allPermissions = $this->permissionData->select('name as text')->get();
                $permission = collect($allPermissions)->map(function ($permission) use ($allModulePermissions) {
                    if ($allModulePermissions->contains('name', $permission->text)) {
                        $permission->state = ['selected' => true];
                    }
                    return $permission;
                });
                return $permission;

            }else{
                return $this->permissionData->select('name as text')->get();
            }
        }catch (\Exception $e){
            Log::error('tree view has permission error : '.$e->getMessage().' line no : '.$e->getLine());
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.parent-links');
    }
}
