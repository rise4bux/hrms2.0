<?php

namespace App\View\Components;

use Illuminate\View\Component;

class RenderChilds extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $childData = [];
    public function __construct($childData)
    {
        $this->childData = $childData;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.render-childs');
    }
}
