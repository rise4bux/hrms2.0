<?php

namespace App\Http\Livewire;

use App\Models\ModuleMaster;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

class GetSubParent extends Component
{
    public $parentsData = [];
    public $counter = 0;

    public function mount($parent_id){
        try {
            /*
             * This method will be called first and will retrieve all parents
             */
            $this->parentsData = [];
            $parentsData = ModuleMaster::where('parent_id',$parent_id);
            if($parentsData->exists()){
                // we will store data by counter as key.
                $this->parentsData[$this->counter] = $parentsData->get()->toArray();
            }
        }catch (\Exception $e){
            Log::error('Get Sub Parent livewire mount method error : '.$e->getMessage().' line no : '.$e->getLine());
        }
    }

    public function createChild($id,$counter){
        try {
            /*
             * This method will fetch and create children according to their parent.
             */
            if(!empty($id)){
                //Check if parent is selected or not.
                $parentsData = ModuleMaster::where('parent_id',$id);
                if($parentsData->exists()){
                    //Check if parent has any child or not.
                    if(array_key_exists($counter,$this->parentsData)){
                        //Here system will check if the selected element's parent already exist or not.
                        $this->resetCounter($counter);
                    }
                    //Add the child data and increment the counter
                    $this->counter++;
                    $this->parentsData[$this->counter] = $parentsData->get()->toArray();

                }else{
                    //Parent not exist, so reset the counter and remove others parents if exists.
                    if(array_key_exists($counter,$this->parentsData)){
                        $this->resetCounter($counter);
                    }
                }
                Log::error('Data = '.json_encode($this->parentsData));
            }else{
                $this->resetCounter($counter);
            }
        }catch (\Exception $e){
            Log::error('Get Sub Parent livewire createChild method error : '.$e->getMessage().' line no : '.$e->getLine());
        }
    }

    public function resetCounter($counter){
        try{
            //we will remove the extra keys and reset the counter
            $this->parentsData = array_slice($this->parentsData,0,$counter+1);
            $this->counter = last(array_keys($this->parentsData));
        }catch (\Exception $e){
              Log::error('Get Sub Parent livewire resetCounter method error : '.$e->getMessage().' line no : '.$e->getLine());
        }
    }

    public function render()
    {
        return view('livewire.get-sub-parent');
    }
}
