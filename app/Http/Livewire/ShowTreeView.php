<?php

namespace App\Http\Livewire;

use App\Models\ModuleMaster;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

class ShowTreeView extends Component
{
    public $parentId;
    public $parentsData = [];
    public $treeViewData = [];
    public $data = [];
    public function __construct() {
        $this->fetchChildrenWithParent();
//        $this->treeViewData = $this->parentsData;
    }

    public function mount($childrens){
//        dd($this->parentsData);
          try {
              if(!empty($childrens)){
                  $this->parentsData = $childrens;
              }
          }catch (\Exception $e){
              Log::error('Show tree view livewire mount method error : '.$e->getMessage().' line no : '.$e->getLine());
          }
    }


    public function fetchChildrenWithParent($id=0){
        try {
            $parentsData = ModuleMaster::where('parent_id',$id);
            if($parentsData->exists()){
                // we will store data
                $parentData = $parentsData->get()->toArray();
                if($id==0) {
                    $this->parentsData = $parentData;
                }
                for ($i=0;$i < count($parentData);$i++){
                    $childData = $this->fetchChildrenWithParent($parentData[$i]['id']);
                    if(!empty($childData)){
                        if($id==0){
                            Log::info('<br> New 0 Index : '.$i.' Count '.count($parentData));
                            $this->parentsData[$i]['isChecked'] = false;
                            $this->parentsData[$i]['child'] = $childData;
                        }else{
                            $parentData[$i]['isChecked'] = false;
                            $parentData[$i]['child'] = $childData;
                        }
                    }else{
                        $parentData[$i]['isChecked'] = false;
                    }
                }
                if($id!=0) {
                    return $parentData;
                }
            }
        }catch (\Exception $e){
            Log::error('Show tree view livewire fetch children with their parent method error : '.$e->getMessage().' line no : '.$e->getLine
                ());
        }
    }

    function checked(){
//        $this->parentsData[0]['child'][0]['isChecked'] = true;
//        $this->parentsData[0]['child'][0]['name'] = 'ABC';
//        Log::info('Child : '.json_encode($this->parentsData));
    }
    function checkedRadio(){
        dd($this);
        if(!empty($this->parentsData[$index]['child'])){
            for($i=0;$i<count($this->parentsData[$index]['child']);$i++){
                $this->parentsData[$index]['child'][$i]['isChecked'] = true;
                if(!empty($this->parentsData[$index]['child'][$i]['child'])){
                    $this->checkedSubChilds($this->parentsData[$index]['child'][$i]['child']);
                }
            }
        }
    }

    function checkedSubChilds(&$childData){
        $childData[0]['isChecked'] = true;
        if(!empty($childData[0]['child'])) {
            $this->checkedSubChilds($childData[0]['child']);
        }
    }

    public function render(){
        return view('livewire.show-tree-view',['parentsData'=>$this->parentsData]);
    }
}
