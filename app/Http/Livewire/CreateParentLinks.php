<?php

namespace App\Http\Livewire;

use App\Models\ModuleMaster;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Livewire\Component;
use Spatie\Permission\Models\Permission;
class CreateParentLinks extends Component
{
    public $treeData;
    public $permissionData;
    public function __construct()
    {
        $id = 0;
        $role = auth()->user()->roles->pluck('id')->first();
        $allParentsData = ModuleMaster::with(['roleModules'=>function($query) use($role){
            $query->where(['role_id' => $role]);
        }])->select('module_masters.*','name as text')
            ->join('role_modules','role_modules.module_id','=','module_masters.id')
            ->where('role_modules.role_id',$role);
//        dd($allParentsData->get());
        $this->fetchChildrenWithParent(auth()->user()->roles->pluck('id')->first());
    }

    public function fetchChildrenWithParent($role,$id=0){
        Log::info('fetchChildrenWithParent called');
        try {
            if(auth()->user()->roles->pluck('name')->first() == 'admin'){
                $allParentsData = ModuleMaster::select('*','name as text')->where('module_masters.parent_id',$id);
            }else{
                $allParentsData = ModuleMaster::with(['roleModules'=>function($query) use($role){
                    $query->where(['role_id' => $role]);
                }])->select('module_masters.*','name as text')
                    ->join('role_modules','role_modules.module_id','=','module_masters.id')
                    ->where([['module_masters.parent_id',$id],['role_modules.role_id',$role]]);
            }

            if($allParentsData->exists()){
                $parentData = $allParentsData->get();
                if($id==0) {
                    $this->treeData = $parentData;
                }

                for ($i=0;$i <count($parentData) ;$i++){
                    $childData = $this->fetchChildrenWithParent($role,$parentData[$i]->id);
                    if(!empty($childData)){
                        if($id==0){
                            $this->treeData[$i]->children = $childData;
                        }else{
                            $parentData[$i]->children = $childData;
                        }
                    }
                }
                if($id!=0) {
                    return $parentData;
                }
            }
        }catch (\Exception $e){
            Log::error('Show tree view fetch children with their parent method error : '.$e->getMessage().' line no : '.$e->getLine());
        }
    }



    public function render()
    {
        return view('livewire.create-parent-links');
    }
}
