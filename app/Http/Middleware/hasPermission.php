<?php

namespace App\Http\Middleware;

use App\Models\ModuleMaster;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Spatie\Permission\Models\Permission;

class hasPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next,$permission)
    {
        if(hasPermission($request->route()->getPrefix(),$permission)){
            return $next($request);
        }else{
            abort(401);
        }
    }
}
