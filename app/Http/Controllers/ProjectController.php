<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class ProjectController extends Controller
{
    public function index(){
        $projectData = Project::with('client')->get();
        return view('projects.index',compact('projectData'));
    }

    public function create($id=null){
        $projectData = [];
        $clientData = Client::get();
        if($id!=null){
            $id = Crypt::decrypt($id);
            $projectData = Project::where('id',$id)->first();
        }
        return view('projects.add',[ 'id' => $id, 'projectData' => $projectData, 'clientData' => $clientData ]);
    }

    public function store(Request $request,$id=null){
        $request->validate([
            'name' => 'required',
            'client_id' => 'required',
        ]);

        if(isset($id) && $id!=null){
            Project::where([ 'id' => $id ])->update($request->only('name','client_id'));
        }else{
            Project::create($request->only('name','client_id'));
        }

        $status = ( $id==null ) ? 'Added' : 'Updated';
        return redirect('/projects')->with('success', 'Project '. $status .' successfully');
    }
    public function delete($id){
        if(isset($id) && $id!=null){
            Project::where([ 'id' => Crypt::decrypt($id) ])->delete();
            return redirect('/projects')->with('success', 'Project deleted successfully');
        }
    }
}
