<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departmentData = Department::latest()->get();
        return view('backend.department.index',compact('departmentData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.department.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:departments',
        ]);
        try {
            Department::create($request->only('name'));
            return redirect()->route('department.index')->with('success', 'Department Added successfully');
        }catch (\Exception $e){
            Log::error(' Department Create Error : '.$e->getMessage().' At Line : '.$e->getLine());
            return back()->with('failure', 'Something went wrong');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departmentData = Department::where('id',Crypt::decrypt($id))->first();
        return view('backend.department.add',compact('id','departmentData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => [
                'required',
                Rule::unique('departments')->ignore(Crypt::decrypt($id))
            ]
        ]);
        try {
            Department::where('id',Crypt::decrypt($id))->update($request->only('name'));
            return redirect()->route('department.index')->with('success', 'Department Updated Successfully');
        }catch (\Exception $e){
            Log::error(' Department Updated Error : '.$e->getMessage().' At Line : '.$e->getLine());
            return back()->with('failure', 'Something went wrong');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Department::where('id',Crypt::decrypt($id))->delete();
            return response()->json(['status'=>1,'message'=>'Department Deleted!'],200);
        }catch (\Exception $e){
            Log::error(' Department Deleted Error : '.$e->getMessage().' At Line : '.$e->getLine());
            return response()->json(['status'=>0,'message'=>'Something went wrong!'],200);
        }
    }
}
