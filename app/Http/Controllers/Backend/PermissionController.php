<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\ModuleMaster;
use App\Models\RoleModules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionController extends Controller
{
    public $treeData = [];
    public $permissionData;
    public function __construct(Permission $permission){
        $this->permissionData = $permission;
    }

    public function index($role = null){
        $firstOrderId = Role::where('name','!=','admin')->orderBy('id', 'asc')->pluck('id')->first();
        $role = $role == null ? $firstOrderId : Crypt::decrypt($role);
        $this->fetchChildrenWithParent($role);
        $treeData = [];
        if(!empty($this->treeData)){
            $treeData = $this->treeData->toArray();
        }
        $allRolesIds = Role::select('id','name')->where('name','!=','admin')->orderBy('id', 'asc')->get();
        return view('backend.permissions.view',compact('treeData','allRolesIds','role'));
    }

    public function create(){
        return view('backend.permissions.add');
    }

    public function store(Request $request){
        if(isset($request->name)){
            Permission::create([ 'name' => $request->name ]);
            return redirect()->route('permission.index')->with('success', 'Permission Added!');
        }else{
            return redirect()->back()->with('failure', 'Something went wrong!');
        }
    }

    public function fetchChildrenWithParent($role,$id=0){
        try {

            $allParentsData = ModuleMaster::with(['roleModules'=>function($query) use($role){
                $query->where(['role_id' => $role]);
            }])->select('*','name as text')->where('parent_id',$id);
            if($allParentsData->exists()){

                $parentData = $allParentsData->get();
                if($id==0) {
                    $this->treeData = $parentData;
                }

                for ($i=0;$i <count($parentData) ;$i++){
                    $childData = $this->fetchChildrenWithParent($role,$parentData[$i]->id);
                    if(!empty($childData)){
                        if($id==0){
                            $this->treeData[$i]->children = $childData;
                        }else{
                            $parentData[$i]->children = $childData;
                        }
                    }else{
                        if($id == 0){
                            $this->treeData[$i]->children = $this->hasPermissions($this->treeData[$i]->roleModules);
                        }else{
                            $parentData[$i]->children = $this->hasPermissions($parentData[$i]->roleModules);
                        }
                    }
                }
                if($id!=0) {
                    return $parentData;
                }
            }
        }catch (\Exception $e){
            Log::error('Show tree view fetch children with their parent method error : '.$e->getMessage().' line no : '.$e->getLine());
        }
    }

    public function hasPermissions($allModulePermissions){
        try {

            if(isset($allModulePermissions) && !empty($allModulePermissions->first())){
                $allModulePermissions = $allModulePermissions->first()->permissions;
                $allPermissions = $this->permissionData->select('name as text')->get();
                $permission = collect($allPermissions)->map(function ($permission) use ($allModulePermissions) {
                    if ($allModulePermissions->contains('name', $permission->text)) {
                        $permission->state = ['selected' => true];
                    }
                    return $permission;
                });
                return $permission;

            }else{
                return $this->permissionData->select('name as text')->get();
            }
        }catch (\Exception $e){
            Log::error('tree view has permission error : '.$e->getMessage().' line no : '.$e->getLine());
        }
    }

    public function saveTreeview(Request $request){
        try {


            $roleId =  Crypt::decrypt(trim($request->roleId));
            $permissions = $this->permissionData->select('name as text')->get()->pluck('text')->values();
            $allSelectedModulesIds = collect($request['treeview'])->pluck('parents')->flatten()->filter(function ($value, $key) { return is_numeric($value); })->unique()->values();
            $modulesWithCurrentRole = RoleModules::where('role_id',$roleId);
            $currentModuleIds = $modulesWithCurrentRole->pluck('module_id');


            if(count($allSelectedModulesIds) <= count($currentModuleIds) ){
                $newModules = collect($currentModuleIds)->diff(collect($allSelectedModulesIds))->values();
                $roleModulesData = $modulesWithCurrentRole->whereIn('module_id',$newModules);
                if(!empty($roleModulesData->get()->toArray()) && count($roleModulesData->get()->toArray()) > 0){
                    $roleModulesData->get()->each(function ($module) use($permissions){
                        $module->revokePermissionTo($permissions->toArray());
                    });
                    $roleModulesData->delete();
                }
            }

            for ( $i=0; $i<count($allSelectedModulesIds); $i++){
                $roleModuleData = RoleModules::firstOrCreate(['role_id'=>$roleId,'module_id'=>$allSelectedModulesIds[$i]]);
                $currentParentPermissions = collect($request['treeview'])->whereIn('text',$permissions)->where('parent',$allSelectedModulesIds[$i])->pluck('text')->values();
                $roleModuleData->revokePermissionTo($permissions->toArray());
                if(count($currentParentPermissions) > 0 ){
                    $roleModuleData->givePermissionTo($currentParentPermissions);
                }
            }

            return response()->json(['status'=>1,'message'=>'Permissions Saved !'],200);
        }catch (\Exception $e){
            Log::error('Save tree view operation : '.$e->getMessage().' line no : '.$e->getLine());
        }

    }
}
