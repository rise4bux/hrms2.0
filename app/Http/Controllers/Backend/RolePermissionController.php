<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolePermissionController extends Controller
{
    public function index(){
        $roleWithPermission = Role::with('permissions')->get();
        return view('backend.role-permission.view',compact('roleWithPermission'));
    }

    public function create(){
        $rolesData = Role::all();
        $permissionData = Permission::all();
        return view('backend.role-permission.add',compact('rolesData','permissionData'));
    }

    public function store(Request $request){
        try{

            $roleData = Role::findById($request->role_id);
            $roleData->syncPermissions($request->permissions);
            return redirect()->route('rolepermission.index')->with('success', 'Role & Permission added successfully');

        }catch (\Exception $e){

            dd($e->getMessage());
            Log::error('Permission store error :'.$e->getMessage().' at line :'.$e->getLine());
            return back()->with('error', 'Something went wrong');

        }
    }
}
