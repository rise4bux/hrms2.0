<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\ModuleMaster;
use App\Models\RoleModules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class ModuleMasterController extends Controller
{
    public $treeData = [];
    public $permissionData;
    public function __construct(Permission $permission){
        $this->permissionData = $permission;
    }
    public function index(){
        $moduleMasterData = ModuleMaster::all();
        return view('backend.module-master.view',compact('moduleMasterData'));
    }

    public function create($id=null){

        $moduleMasterData = [];
        if($id!=null){
            $id = Crypt::decrypt($id);
            $moduleMasterData = ModuleMaster::where('id',$id)->first();
        }
        return view('backend.module-master.add',compact('moduleMasterData','id'));
    }

    public function store(Request $request,$id=null){

        $validated = $request->validate([
           'moduleName' => 'required',
           'parent_id' => 'nullable',
           'url' => 'nullable',
       ]);

        try {
            if($id!=null){
                ModuleMaster::where('id',$id)->update([
                    'name'=>$request->moduleName,
                    'module_url'=>$request->url,
                ]);
                return redirect()->route('modulemaster.index')->with('success', 'Module updated successfully');
            }else{
                if(isset($request->parentsData) && is_array($request->parentsData) ){
                    if(count($request->parentsData) > 1){
                        $parent_id = (last($request->parentsData) == NULL ) ? $request->parentsData[ count($request->parentsData) - 2 ] : last($request->parentsData);
                    }else{
                        $parent_id = (last($request->parentsData) == NULL ) ? 0 : $request->parentsData[0];
                    }
                }else{
                    $parent_id = 0;
                }

                ModuleMaster::create([
                    'name'=>$request->moduleName,
                    'parent_id'=> $parent_id,
                    'module_url'=>$request->url,
                ]);
                return redirect()->route('modulemaster.index')->with('success', 'Module added successfully');
            }
        }catch (\Exception $e){

            Log::error("Message :".$e->getMessage()." Error at :".$e->getLine());

        }

    }

    public function delete($id){
        try{
            if($id!=null){
                $id = Crypt::decrypt($id);
                ModuleMaster::where('id',$id)->delete();
                return redirect()->route('modulemaster.index')->with('success', 'Module Deleted !');
            }else{
                return redirect()->back()->with('failure', 'Something went wrong!');
            }
        }catch (\Exception $e){
            Log::error('Module Delete Error : '.$e->getMessage().' Line At : '.$e->getLine());
        }
    }

    public function fetchChildrenWithParent($id=0){
           try {
               $roleId = auth()->user()->roles()->pluck('id');
               $allParentsData = ModuleMaster::with('roleModules.permissions')->select('*','name as text')->where('parent_id',$id);
               $modulesWithCurrentRole = RoleModules::where('role_id',$roleId);
               $roleModuleIds = $modulesWithCurrentRole->pluck('id')->toArray();
               if($allParentsData->exists()){
                   // we will store data
                   $parentData = $allParentsData->get();
                   if($id==0) {
                       $this->treeData = $parentData;
                   }

                   for ($i=0;$i <count($parentData) ;$i++){
                       $childData = $this->fetchChildrenWithParent($parentData[$i]->id);
                       if(!empty($childData)){
                           if($id==0){
                               $this->treeData[$i]->children = $childData;
                           }else{
                               $parentData[$i]->children = $childData;
                           }
                       }else{
                           if($id == 0){
                               $this->treeData[$i]->children = $this->hasPermissions($this->treeData[$i]->roleModules);
                           }else{
                               $parentData[$i]->children = $this->hasPermissions($parentData[$i]->roleModules);
                           }
                       }
                   }
                   if($id!=0) {
                       return $parentData;
                   }
               }
           }catch (\Exception $e){
               Log::error('Show tree view fetch children with their parent method error : '.$e->getMessage().' line no : '.$e->getLine());
           }
   }

    public function hasPermissions($allModulePermissions){
        try {
            if(isset($allModulePermissions) && !empty($allModulePermissions->first())){
                $allModulePermissions = $allModulePermissions->first()->permissions;
                $allPermissions = $this->permissionData->select('name as text')->get();
                $permission = collect($allPermissions)->map(function ($permission) use ($allModulePermissions) {
                    if ($allModulePermissions->contains('name', $permission->text)) {
                        $permission->state = ['selected' => true];
                    }
                    return $permission;
                });
                return $permission;

            }else{
                return $this->permissionData->select('name as text')->get();
            }
        }catch (\Exception $e){
            Log::error('tree view has permission error : '.$e->getMessage().' line no : '.$e->getLine());
        }
    }

    public function saveTreeview(Request $request){
        try {

            $permissions = $this->permissionData->select('name as text')->get()->pluck('text')->values();
            $roleId = auth()->user()->roles()->pluck('id')->first();
            $allSelectedModulesIds = collect($request['treeview'])->pluck('parents')->flatten()->filter(function ($value, $key) { return is_numeric($value); })->unique()->values();

            $modulesWithCurrentRole = RoleModules::where('role_id',$roleId);
            $currentModuleIds = $modulesWithCurrentRole->pluck('module_id');
            if(count($allSelectedModulesIds) < count($currentModuleIds) ){
                $newModules = collect($currentModuleIds)->diff(collect($allSelectedModulesIds))->values();
                $roleModulesData = $modulesWithCurrentRole->whereIn('module_id',$newModules);

                $roleModulesData->get()->each(function ($module) use($permissions){
                    $module->revokePermissionTo($permissions->toArray());
                });
                $roleModulesData->delete();
            }

            for ( $i=0; $i<count($allSelectedModulesIds); $i++){
                $roleModuleData = RoleModules::firstOrCreate(['role_id'=>$roleId,'module_id'=>$allSelectedModulesIds[$i]]);
                $currentParentPermissions = collect($request['treeview'])->whereIn('text',$permissions)->where('parent',$allSelectedModulesIds[$i])->pluck('text')->values();
                $roleModuleData->revokePermissionTo($permissions->toArray());
                if(count($currentParentPermissions) > 0 ){
                    $roleModuleData->givePermissionTo($currentParentPermissions);
                }
            }

            return response()->json(['status'=>1,'message'=>'Permissions Saved !'],200);
        }catch (\Exception $e){
            Log::error('Save tree view operation : '.$e->getMessage().' line no : '.$e->getLine());
        }

    }
}
