<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\Project;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tagData = Tag::latest()->get();

        return view('backend.tags.index',compact('tagData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projectData = Project::all();
        return view('backend.tags.add',compact('projectData'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required|unique:tags',
            'project_id'=>'required',
        ]);

        try {
            Tag::create($request->only('name','project_id'));
            return redirect()->route('tags.index')->with('success', 'Tag Added successfully');
        }catch (\Exception $e){
            Log::error('Tag Store Error : '.$e->getMessage().' At Line : '.$e->getLine());
            return back()->with('failure', 'Something went wrong');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tagData = Tag::where('id',Crypt::decrypt($id))->first();
        $projectData = Project::all();
        return view('backend.tags.add',compact('id','tagData','projectData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => [
                'required',
                Rule::unique('tags')->ignore(Crypt::decrypt($id))
            ],
            'project_id'=>'required',
        ]);
        try {
            Tag::where('id',Crypt::decrypt($id))->update($request->only('name','project_id'));
            return redirect()->route('tags.index')->with('success', 'Tag Updated Successfully');
        }catch (\Exception $e){
            Log::error('Tag Updated Error : '.$e->getMessage().' At Line : '.$e->getLine());
            return back()->with('failure', 'Something went wrong');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Tag::where('id',Crypt::decrypt($id))->delete();
            return response()->json(['status'=>1,'message'=>'Tag Deleted!'],200);
        }catch (\Exception $e){
            Log::error(' Tag Deleted Error : '.$e->getMessage().' At Line : '.$e->getLine());
            return response()->json(['status'=>0,'message'=>'Something went wrong!'],200);
        }
    }
}
