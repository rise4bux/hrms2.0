<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{

    public function index(){

        return view('backend.users.view');

    }

    public function create(){

        $rolesData = Role::all();
        $permissionData = Permission::all();
        return view('backend.users.add',compact('rolesData','permissionData'));

    }

    public function store(Request $request){

        try{
            $validated = $request->validate([
                'email' => 'required|max:255',
                'password' => 'required|min:8',
                'confirmPassword' => 'required|min:8|same:password',
            ]);
            User::create(
                [
                    'name'=>$request->name,
                    'email'=>$request->email,
                    'password'=>Hash::make($request->password),
                ]
            );
            return back()->with('success', 'User added successfully');

        }catch (\Exception $e){

            Log::error('User create error :'.$e->getMessage().' at line :'.$e->getLine());
            return back()->with('error', 'Something went wrong');

        }

    }

}
