<?php

namespace App\Http\Controllers\Backend;

use App\GlobalSetting;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\EmployeeUpdateRequest;
use App\Mail\EmployeeBlock;
use App\Mail\EmployeeRegister;
use App\Models\Company;
use App\Models\Employee;
use App\Models\GroupEmployee;
use App\Models\WebShopItem;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class WebShopController extends Controller
{
    public function index()
    {
        try {
            return view('backend.webshop-item.index');
        } catch (\Exception $e) {
            Log::error('People management error:' . $e->getMessage());
            return redirect()->route('dashboard')->with('failure', 'Something went wrong');
        }
    }

    public function ajaxTable(Request $request)
    {
        try {
            $inputs = $request->only('query');
            $search = '';
            if (!empty($inputs['query']) && !empty($inputs['query']['generalSearch'])) {
                if (isset($inputs['query']['generalSearch']) && !empty($inputs['query']['generalSearch'])) {
                    $search = $inputs['query']['generalSearch'];
                }
                $items = WebShopItem::where(function ($query) use ($search) {
                    $query->where('Description', 'LIKE', "%$search%");
                    $query->orWhere(function ($query) use ($search) {
                        $query->where('Description_2', 'LIKE', "%$search%");
                    });
                    $query->orWhere(function ($query) use ($search) {
                        $query->where('Manufacturer_Item_No', 'LIKE', "%$search%");
                    });
                    $query->orWhere(function ($query) use ($search) {
                        $query->where('EAN_No', 'LIKE', "%$search%");
                    });
                    $query->orWhere(function ($query) use ($search) {
                        $query->where('Inventory', 'LIKE', "%$search%");
                    });
                    $query->orWhere(function ($query) use ($search) {
                        $query->where('Manufacturer_Name', 'LIKE', "%$search%");
                    });
                })->orderBy('id', 'desc')->latest()->get();
            }
            else{
                $items = WebShopItem::all();
            }
            return response()->json(['status' => 'success', 'data' => $items, 'code' => 200], 200);
        } catch (\Exception $e) {
            Log::error('People management error:' . $e->getMessage());
            return '';
        }
    }

    public function create()
    {
        return view('backend.webshop-item.form');
    }

    public function store(Request $request)
    {
        $values = $request->except('_token');
        try {
            $items = WebShopItem::create($values);
            return redirect()->route('webshop-item.index')->with('success', 'item details stored!');
        } catch (\Exception $e) {
            Log::error('People management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('webshop-item.index')->with('failure', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        try {
            $items = WebShopItem::where('id', $id)->first();
            return view('backend.webshop-item.form', compact('items'));
        } catch (\Exception $e) {
            Log::error('People management error:' . $e->getMessage());
            return redirect()->route('webshop-item.index')->with('failure', 'Something went wrong');
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $values = $request->only(['Unit_Price', 'Price24', 'Price48']);
            $values['is_updated'] = 1;
            WebShopItem::where('id', $id)->update($values);
            return redirect()->route('webshop-item.index')->with('success', 'item details updated!');
        } catch (\Exception $e) {
            Log::error('People management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('webshop-item.index')->with('failure', 'Something went wrong');
        }
    }

    public function deleteItem($id)
    {
        try {
            WebShopItem::where('id', $id)->delete();
            return redirect()->route('webshop-item.index')->with('success', 'Item Deleted!');
        } catch (\Exception $e) {
            Log::error('People management error:' . $e->getMessage());
            return redirect()->route('webshop-item.index')->with('failure', 'Something went wrong');
        }
    }

    public function updateWebshopItems()
    {
        try{
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', 'https://T1005316.dynamicstocloud.com:1173/ST-118623/ODataV4/Company(\'DEMO\')/WebshopItem', [
                'auth' => ['DYNAMICSTOCLOUD\SPU2007165641', 'F-5yTE=N4-CSfADb', 'digest'],
                'headers' => ['Host' => 't1005316.dynamicstocloud.com:1173'],
            ]);
            $content = $response->getBody()->getContents();
            $values = collect(json_decode($content))->map(function ($item, $key) {
                if ($key == "value") {
                    return $item;
                }
            })->toArray();
//        \Illuminate\Support\Facades\DB::table('web_shop_items')->truncate();
            foreach ($values['value'] as $v) {
                $array = collect($v)->toArray();
                $exist = WebShopItem::where('Manufacturer_Item_No', $array['Manufacturer_Item_No'])->where('EAN_No', $array['EAN_No'])->first();
                if ($exist == null) {
                    DB::table('web_shop_items')->insert($array);
                }else{
                    if($exist['is_updated'] == 1){
                        unset($array['Unit_Price']);
                        unset($array['Price24']);
                        unset($array['Price48']);
                    }
                    $exist->update($array);
                }
            }
            return response()->json(['status'=>1,'message'=>'updated success'],200);
        }catch (\Exception $e){
            Log::info('Web shop item update Error==>'.json_encode($e));
            return response()->json(['status'=>0,'message'=>'updated failed'],422);
        }
        }
}
