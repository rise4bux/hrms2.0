<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\MenuContent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MenuContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $menuContentData = MenuContent::get()->groupBy('menu_id');
            return view('backend.menu-content.index',compact('menuContentData'));
        }
        catch (\Exception $e){
            Log::error('Menu content fetch error :'.$e->getMessage().' at line :'.$e->getLine());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            if(isset($request->section) && !empty($request->section)){
                    $i = 1;
                    foreach($request->section as $row){

                        if($row['html'] != null){
                            $menuContentData = MenuContent::where([['menu_id',$request->menu_id],['section_sequence',$i]]);
                            if($menuContentData->exists()){
                                //Update the content if already exists
                                $menuContentData->update([
                                    'section_name' => $row['name'],
                                    'content' => $row['html'],
                                ]);
                            }else{
                                //add the content
                                MenuContent::create([
                                    'menu_id' => $request->menu_id,
                                    'section_sequence' => $i,
                                    'section_name' => $row['name'],
                                    'content' => $row['html'],
                                ]);
                            }
                        }
                        $i++;
                }
            }
            return back()->with('success', 'All section updated successfully !');
        } catch (\Exception $e){
            Log::error('Menu content store add/update :'.$e->getMessage().' at line :'.$e->getLine());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getHomePageContent(){
        //1 refers to home menu
        $homeMenuData = MenuContent::select('section_sequence','section_name','content')->where('menu_id','1');
        if($homeMenuData->exists()){
            $data = $homeMenuData->orderBy('section_sequence')->get()->groupBy('section_name');
            $response['status'] = 1;
            $response['message'] = 'Data fetched successfully';
            $response['data'] = $data;
            return response()->json($response,200);
        }else{
            $response['status'] = 0;
            $response['message'] = 'No data found!';
            $response['data'] = [];
            return response()->json($response,422);
        }
    }
    public function getVelgenPageContent(){
        //2 refers to home menu
        $homeMenuData = MenuContent::select('section_sequence','section_name','content')->where('menu_id','2');
        if($homeMenuData->exists()){
            $data = $homeMenuData->orderBy('section_sequence')->get()->groupBy('section_name');
            $response['status'] = 1;
            $response['message'] = 'Data fetched successfully';
            $response['data'] = $data;
            return response()->json($response,200);
        }else{
            $response['status'] = 0;
            $response['message'] = 'No data found!';
            $response['data'] = [];
            return response()->json($response,422);
        }
    }

}
