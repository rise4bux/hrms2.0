<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function index(){
        try {
            $allRoles = Role::latest()->get();
            return view('backend.roles.view',compact('allRoles'));
        }catch (\Exception $e){
            Log::error('Role Index Error : '.$e->getMessage().' Line At : '.$e->getLine());
        }

    }

    public function create($id=null){
        try{
            $roleData = [];
            if($id!=null){
                $id = Crypt::decrypt($id);
                $roleData = Role::where('id',$id)->first();
            }
            return view('backend.roles.add-role',compact('id','roleData'));
        }catch (\Exception $e){
            Log::error('Role Create Error : '.$e->getMessage().' Line At : '.$e->getLine());
        }
    }

    public function store(Request $request,$id=null){
        try{
            if(isset($request->name)){
                if($id == null){
                    Role::create([ 'name' => $request->name ]);
                }else{
                    Role::where('id',$id)->update([ 'name' => $request->name ]);
                }
                $status = ( $id==null ) ? 'Added' : 'Updated';
                return redirect()->route('role.index')->with('success', 'Role '.$status.'!');
            }else{
                return redirect()->back()->with('failure', 'Something went wrong!');
            }
        }catch (\Exception $e){
            Log::error('Role Store Error : '.$e->getMessage().' Line At : '.$e->getLine());
        }
    }
    public function delete($id){
        try{
            if($id!=null){
                $id = Crypt::decrypt($id);
                Role::where('id',$id)->delete();
                return redirect()->route('role.index')->with('success', 'Role Deleted !');
            }else{
                return redirect()->back()->with('failure', 'Something went wrong!');
            }
        }catch (\Exception $e){
            Log::error('Role Delete Error : '.$e->getMessage().' Line At : '.$e->getLine());
        }
    }
}
