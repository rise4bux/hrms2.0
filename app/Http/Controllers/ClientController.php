<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class ClientController extends Controller
{
    public function index(){
        $clientData = Client::all();
        return view('clients.index',compact('clientData'));
    }

    public function create($id=null){
        $clientData = [];
        if($id!=null){
            $id = Crypt::decrypt($id);
            $clientData = Client::where('id',$id)->first();
        }
        return view('clients.add',[ 'id' => $id, 'clientData' => $clientData ]);
    }

    public function store(Request $request,$id=null){
        $request->validate([
            'name' => 'required',
        ]);

        if(isset($id) && $id!=null){
            Client::where([ 'id' => $id ])->update($request->only('name'));
        }else{
            Client::create($request->only('name'));
        }

        $status = ( $id==null ) ? 'Added' : 'Updated';
        return redirect('/clients')->with('success', 'Client '. $status .' successfully');
    }

    public function delete($id){
        if(isset($id) && $id!=null){
            Client::where([ 'id' => Crypt::decrypt($id) ])->delete();
            return redirect('/clients')->with('success', 'Client deleted successfully');
        }
    }

}
