<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Employee;
use App\Models\ModuleMaster;
use App\Models\RoleModules;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Role;

class EmployeeController extends Controller
{
    public function index(Request $request){
        try {
            $allRoles = Role::all()->where('name','!=','admin')->pluck('name')->toArray();
            $employee = User::role($allRoles)->latest()->get();
            return view('backend.employee.view',compact('employee'));
        }catch (\Exception $e){
            Log::error('Employee Index Error : '.$e->getMessage().' Line At : '.$e->getLine());
        }
    }

    public function create(Request $request,$id=null){
        $employeeData = [];
        $departmentData = Department::get();
        if($id!=null){
            $employeeData = User::with('roles')->where('id', Crypt::decrypt($id))->first();
        }
        $roles = Role::where('name','!=','admin')->get(['id','name']);
        return view('backend.employee.add',compact('roles','employeeData','id','departmentData'));
    }

    public function store(Request $request,$id=null){
        $request->validate([
            'name' => 'required',
            'email' => 'required',
        ]);
        $employeeData = $request->only('name','email','password','role');
        $role = $employeeData['role'];
        unset($employeeData['role']);
        if($id==null){
            $employeeData['password'] = Hash::make($employeeData['password']);
            $user = User::create($employeeData);
            $user->assignRole($role);
            return redirect('employee/')->with(['success'=>'Employee Added successfully']);
        }else{
            $id = Crypt::decrypt($id);
            $user = User::with('roles')->where('id',$id);
            $user->update($employeeData);
            $user->first()->removeRole($user->first()->roles()->first()->name);
            $user->first()->assignRole($role);

            return redirect('employee/')->with(['success'=>'Employee Updated successfully']);
        }
    }
}
