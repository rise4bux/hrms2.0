<?php

namespace App\Http\Controllers\Auth;

use App\GlobalSetting;
use App\Http\Requests\Backend\UserProfileRequest;
use App\Models\Company;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
class ProfileController extends Controller
{
    protected $repository;

    public function showProfile()
    {
        try {
         $user=User::where('id',auth()->id())->first();
            // dd($user);
            return view('backend.auth.profile', compact('user'));
        } catch (\Exception $e) {
            Log::error('Profile error:'.$e->getMessage());
            return redirect()->back()->with('failure', 'Something went wrong');
        }
    }

    public function updateProfile(Request $request)
    {

        try {

            $input = $request->only('name', 'profile_avatar','email','profile_avatar_remove');

            $checkExist = User::where('email',$input['email'])->where('id','!=',auth()->id())->first();
            if(!empty($checkExist)) {
                return redirect()->back()->withInput()->with('failure', 'Email already Exists');
            }

            $file = $request->file('profile_avatar');
            if($file) {
                $destinationPath = 'profile-image/';
                $originalFile = $file->getClientOriginalName();
                $filename= rand(1,100).$originalFile;
                $file->move($destinationPath, $filename);
            }
            $user =User::where('id',auth()->id())->first();

            if(!empty($request->file('profile_avatar'))) {
                $input['logo'] = $filename;

                if(!empty($user->logo)) {

                    File::delete(public_path('profile-image/'. $user->logo));

                }
            }
                if($input['profile_avatar_remove'] == "1" ){
                if(!empty($user->logo)) {

                    File::delete(public_path('profile-image/'. $user->logo));

                }
                $input['logo'] = '';
            }

            $user->update([
               'name'=>$input['name'],
               'email'=>$input['email'],
               'logo'=>$input['logo'],
            ]);
            return redirect()->route('view.profile')->with('success', 'Profile updated successfully');

        } catch (\Exception $e) {

            Log::error('Profile error:'.$e->getMessage());
            return redirect()->back()->with('failure', 'Something went wrong');
        }

    }

    public function changePassword()
    {
        $user = User::where('id',auth()->id())->first();
        return view('backend.auth.change_password', compact('user'));
    }
    public function updatePassword(Request $request)
    {
        try {

            $request->validate([
                'current_password' => ['required'],
                'new_password' => ['required'],
                'confirm_password' => ['same:new_password'],
            ]);
            if(Hash::check($request->current_password, auth()->user()->password)==false){
                return redirect()->back()->with('failure', 'Current password does not match!');
            }
            $input =  $input = $request->only('new_password');

            $data['password'] =Hash::make($input['new_password']);
            $user = User::where('id',\auth()->id())->update($data);
            return redirect()->route('view.profile')->with('success', 'Password updated successfully');
        }
        catch (\Exception $e) {
            Log::error('Profile error:'.$e->getMessage());
            return redirect()->back()->with('failure', 'Something went wrong');
        }
    }
    public function editAddress() {
        $user = $this->repository->edit();
        $userCompany = Company::where('user_id',$user->id)->first();
        return view('backend.auth.change_address', compact('user','userCompany'));
    }
    public function updateAddress(Request $request)
    {

        try {
            $input = $request->only('address', 'city','postal_code','country','vat');

            if(Auth::user()->hasRole(['company'])) {

                $userCompany = Company::where('user_id',auth()->id())->first();
                $company = Company::find($userCompany->id)->update($input);
                return redirect()->route('view.profile')->with('success', 'Address updated successfully');
            }


        } catch (\Exception $e) {

            Log::error('Profile error:'.$e->getMessage());
            return redirect()->back()->with('failure', 'Something went wrong');
        }

    }

    public function updateOwnerEmail(Request $request){
        try {
            $request->validate([
                'email' => 'required|email',
            ]);
            $input = $request->only('email');
            $user = User::find(auth()->user()->id)->update($input);
            return redirect()->route('view.profile')->with('success', 'Email updated successfully');

        } catch (\Exception $e) {
            Log::error('Profile Owner email update error:'.$e->getMessage());
            return redirect()->back()->with('failure', 'Something went wrong');
        }
    }
//    public function settings() {
//        if(Auth::user()->hasRole(['admin'])){
//            $settings=  GlobalSetting::all();
//            return view('backend.auth.admin-settings',compact('settings'));
//        }
//        $user = $this->repository->edit();
//        $userCompany = Company::where('user_id',$user->id)->first();
//        $settings = Setting::where('company_id',$userCompany->id)->first();
//        $android_app_link=GlobalSetting::where('key','android_app_link')->value('value');
//        $ios_app_link=GlobalSetting::where('key','ios_app_link')->value('value');
//        return view('backend.auth.settings', compact('user','userCompany','settings','android_app_link','ios_app_link'));
//    }
//    public function updateSettings(Request $request) {
//        $input = $request->only(
//            'hr_email',
//            'low',
//            'medium',
//            'high',
//            'low_color',
//            'medium_color',
//            'high_color',
//            'guest_user_link',
//            'ios_app_link',
//            'privacy1',
//            'privacy2',
//            'privacy3',
//            'information'
//        );
//
//        $userCompany = Company::where('user_id',auth()->id())->first();
//        if(!empty($input['hr_email'])) {
//
//            $settings = Setting::where('company_id',$userCompany->id)->first();
//            if(!empty($settings)) {
//                Setting::find($settings->id)->update($input);
//            }
//            else {
//                $input['company_id'] = $userCompany->id;
//                $setting = Setting::create($input);
//            }
//            return redirect()->route('view.profile')->with('success', 'Setting updated successfully');
//        }
//    }
//    public function updateAdminSettings(Request $request){
//        GlobalSetting::truncate();
//        if($request->values){
//            foreach ($request->values as $option){
//                GlobalSetting::create($option);
//            }
//            return redirect()->route('view.profile')->with('success', 'Setting updated successfully');
//        }
//
//    }
}
