<?php

namespace App\Http\Controllers;

use App\Models\RoleModules;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class RoleModuleController extends Controller
{
    public function index(){
        $roleModuleData = RoleModules::where('module_id',21)->with('currentUserRole')->first();

        dd($roleModuleData,$roleModuleData->hasPermissionTo('add'),'Is admin : '.auth()->user()->hasRole($roleModuleData['currentUserRole']['name']));
    }
}
