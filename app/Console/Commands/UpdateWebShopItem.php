<?php

namespace App\Console\Commands;

use App\Http\Controllers\Backend\WebShopController;
use App\Http\Controllers\WebshopApiController;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class UpdateWebShopItem extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateWebShopItemsCommand:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $webShopController = new WebShopController();
            $webShopController->updateWebshopItems();
            Log::info('updateWebShopItems cron is now working');
        }
        catch (\Exception $e){
            Log::error('Update web shop crob error:'.$e->getMessage().' at line :'.$e->getLine());
        }
    }
}
