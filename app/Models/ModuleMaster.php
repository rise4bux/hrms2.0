<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class ModuleMaster extends Model
{
    use HasFactory,HasRoles;
    protected $guarded = [];
    protected $guard_name = 'web';

    function roleModules(){
        return $this->hasMany(RoleModules::class,'module_id','id');
    }
}
