<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class RoleModules extends Model
{
    use HasFactory,HasRoles;
    protected $guarded = [];
    protected $guard_name = 'web';
    public function currentUserRole(){
        return $this->belongsTo(\Spatie\Permission\Models\Role::class,'role_id','id')->where('name',auth()->user()->roles()->pluck('name')->first());
    }

//    public function ScopeHasUserRole($query){
//        $query->where('');
//    }
}
