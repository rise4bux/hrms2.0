<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenuContent extends Model
{
    use HasFactory,SoftDeletes;
    protected $fillable = [
        'menu_id',
        'section_sequence',
        'section_name',
        'content'
    ];
}
