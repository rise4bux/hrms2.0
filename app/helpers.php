<?php

use App\Models\ModuleMaster;
use Spatie\Permission\Models\Permission;

function getPermissionRouteWise($routeName){

    $currentUserRoleId = auth()->user()->roles->pluck('id')->first();
    $moduleMaster = ModuleMaster::with(['RoleModules'=>function($query) use($currentUserRoleId){ $query->where('role_id',$currentUserRoleId); }])->where('module_url',$routeName)->first();
    $currentPermission = $moduleMaster->RoleModules->first()->permissions()->get();
    if(empty($currentPermission)){
        abort(404);
    }else{
        return $currentPermission;
    }
}

function hasPermission($routeName,$individualPermission = null){
    dd($routeName);
    if(auth()->user()->roles->pluck('name')->first() == 'admin'){
        return true;
    }else{
        $currentUserRoleId = auth()->user()->roles->pluck('id')->first();
        $moduleMaster = ModuleMaster::with(['RoleModules'=>function($query) use($currentUserRoleId){ $query->where('role_id',$currentUserRoleId); }])->where('module_url',$routeName)->first();
        if(!empty($moduleMaster)){
            if(count($moduleMaster->RoleModules) > 0){
                $permissions = Permission::pluck('name')->toArray();
                if(!empty($permissions)){
                    if($individualPermission == null){
                        return $moduleMaster->RoleModules->first()->hasAnyPermission($permissions);
                    }else{
                        return $moduleMaster->RoleModules->first()->hasPermissionTo($individualPermission);
                    }
                }
            }
        }
        return false;
    }
}

?>