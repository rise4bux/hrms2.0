<?php

namespace App\Providers;

use App\Http\Middleware\hasPermission;
use App\Models\RoleModules;
use App\View\Components\RenderChilds;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Blade::if('canModule', function ($permissoin) {
            return hasPermission(Route::getCurrentRoute()->uri,$permissoin);
        });


    }
}
