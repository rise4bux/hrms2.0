<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name'=>'add']);
        Permission::create(['name'=>'edit']);
        Permission::create(['name'=>'delete']);
        Permission::create(['name'=>'read']);
        Permission::create(['name'=>'update']);
        Permission::create(['name'=>'csv']);
    }
}
