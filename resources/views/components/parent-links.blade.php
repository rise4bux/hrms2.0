
@if(!empty($treeData))

    @foreach($treeData as $row)
        <li class="menu-item menu-item-submenu" aria-haspopup="true">
            <a href="javascript:;" class="menu-link menu-toggle">
                <span class="menu-text">{{ $row->name }}</span>
                @if(count($row->children) > 0)<i class="menu-arrow"></i>@endif
            </a>
            @if(count($row->children) > 0)
                <div class="menu-submenu menu-submenu-classic menu-submenu-right">
                    <ul class="menu-subnav">
                        <x-create-links :treeData="$row->children" />
                    </ul>
                </div>
            @endif
        </li>
    @endforeach
@endif
