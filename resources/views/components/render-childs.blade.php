@if(!empty($childData))
    <ul class="tv-left-border">
        @foreach($childData as $row)
            <li><input type="checkbox"
                    wire:key="{{ $loop->index }}" wire:click="checkedRadio({{ $loop->index }})"
                    {{ isset($row['isChecked']) && $row['isChecked'] ? 'checked' : ''}} >
                    {{ $row['name'] }}
            </li>
            @if(isset($row['child']))
                <x-render-childs :childData="$row['child']" />
            @endif
        @endforeach
     </ul>
@endif

