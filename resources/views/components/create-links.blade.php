@if(!empty($treeData))
    <div class="menu-submenu menu-submenu-classic menu-submenu-left">
        <ul class="menu-subnav">
        @foreach($treeData as $row)
                <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                    <a href="javascript:;" class="menu-link menu-toggle">
                        <span class="svg-icon menu-icon"></span>
                        <span class="menu-text">{{ $row->name }}</span>
                        @if($row->children !=null && count($row->children) > 0)<i class="menu-arrow"></i>@endif
                        @if($row->children!=null && count($row->children) > 0)
                            <x-create-links :treeData="$row->children" />
                        @endif
                    </a>
                </li>
        @endforeach
        </ul>
    </div>
@endif

