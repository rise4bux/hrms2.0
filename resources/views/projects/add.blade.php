
@section('title','Add Project')
@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5">Add Project</h5>
                        <!--end::Page Title-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Daterange-->
                    <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="" data-toggle="tooltip" title="" data-placement="left">
                        <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                        <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                    </a>
                    <!--end::Daterange-->
                    <!--begin::Dropdown-->
                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <div class="row">
                <div class="col-lg-12">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">Add Project</h3>
                        </div>
                        <!--begin::Form-->
                        <form  class="form m-form m-form--fit m-form--label-align-right"
                               action="{{ (isset($id) && $id != null) ? route('projects.update').'/'.$id : route('projects.store') }}"
                               method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body row">
                                <div class="col-6">
                                    <div class="form-group col-lg-12">
                                        <label class="form-control-label">Name</label>
                                        <input type="text" name="name" class="form-control" value="{{ isset($projectData) && !empty($projectData) ? $projectData->name : '' }}" >
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group col-lg-12">
                                        <label class="form-control-label">Select Clients</label>
                                            <select class="form-control" name="client_id" >
                                                @foreach($clientData as $row)
                                                    <option value="{{ $row->id }}" {{ isset($projectData) && !empty($projectData) && $row->id == $id ? 'selected' : ''  }} >{{ $row->name }}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button type="submit" class="btn btn-success font-weight-bold mr-2">{{  isset($projectData) && !empty($projectData) ? 'Update' : 'Add' }}</button>
                                </div>
                            </div>
                            <!--end::Code example-->
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <!--end::Card-->
                </div>
            </div>
        </div>
        <!--end::Content-->
    </div>
@endsection

