<div>
@if(!empty($parentsData))
 @if($parentsData[0]['parent_id'] != 0) <li> @endif
        <ul class="{{ $parentsData[0]['parent_id'] != 0 ? 'tv-left-border' : '' }}">
            @foreach($parentsData as $row)
                <li><input type="checkbox" wire:click="checkedRadio()"
                        {{ isset($row['isChecked']) && $row['isChecked'] ? 'checked' : ''}} >
                        {{ $row['name'] }}
                </li>
                @if(isset($row['child']))
                    <x-render-childs :childData="$row['child']" />
                @endif
            @endforeach
         </ul>
 @if($parentsData[0]['parent_id'] != 0) </li> @endif
@endif
</div>
