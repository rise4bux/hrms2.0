 @if(!empty($treeData))
@foreach($treeData as $row)
    <li class="menu-item menu-item-submenu menu-item-rel {{ request()->segment(1) == $row->module_url  ? 'menu-item-active' : '' }}" data-menu-toggle="click" aria-haspopup="true">
        <a href="{{ $row->module_url == null ? 'javascript:;' : url($row->module_url).'/' }}" class="menu-link  {{ ($row->children !=null && count($row->children) > 0) ? 'menu-toggle' : '' }}">
            <span class="menu-text">{{ $row->name }}</span>
            @if($row->children !=null && count($row->children) > 0)<i class="menu-arrow"></i>@endif
        </a>
        @if($row->children !=null && count($row->children) > 0)
            <x-create-links :treeData="$row->children" />
        @endif
    </li>
@endforeach
@endif