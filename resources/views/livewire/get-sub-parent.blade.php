<div>
    @foreach($parentsData as $key => $value)
        <div class="form-group row">
        <label for="parent_module" class="col-2 col-form-label">Parent Module</label>
        <div class="col-10">
            <select class="form-control"
                    name="parentsData[]" wire:change.prevent="createChild($event.target.value,{{ $key }})" >
                <option value="" selected>Select Parent</option>
                @foreach($value as $row)
                    <option value="{{ $row['id'] }}" >{{ $row['name'] }}</option>
                @endforeach
            </select>
      </div>
    </div>
    @endforeach
</div>
