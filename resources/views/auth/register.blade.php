



@extends('backend.layouts.auth_master')

@section('content')
    <div class="login login-4 wizard d-flex flex-column flex-lg-row flex-column-fluid wizard" id="kt_login" data-wizard-state="first">
        <!--begin::Content-->
        <div class="login-container d-flex flex-center flex-row flex-row-fluid order-2 order-lg-1 flex-row-fluid bg-white py-lg-0 pb-lg-0 pt-15 pb-12">
            <!--begin::Wrapper-->
            <!--begin::Signin-->
            <div class="login-content login-content-signup d-flex flex-column">
                <!--begin::Logo-->

                <div class="d-flex flex-column-auto flex-column px-10">
                    <!--begin::Aside header-->
                    <a href="{{ route('login') }}" class="login-logo pb-lg-4 pb-10">
                        {{--<img src="{{ asset('theme/media/logos/logo-4.png') }}" class="max-h-70px" alt="">--}}
                        <img src="{{ asset('images/site_logo.png') }}" class="max-h-150px" alt="">
                    </a>
                    <!--end::Aside header-->

                    <!--begin: Wizard Nav-->

                    <!--end: Wizard Nav-->
                </div>
                <div class="login-form">
                    <!--begin::Form-->

                    <form class="form px-10 fv-plugins-bootstrap fv-plugins-framework" novalidate="novalidate" id="kt_login_signup_form"  method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                    @csrf
                    <!--begin: Wizard Step 1-->
                        <div class=" " data-wizard-type="step-content" data-wizard-state="current">
                            <!--begin::Title-->
                            <div class="pb-10 pb-lg-12">
                                <h3 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Create Account</h3>
                                <div class="text-muted font-weight-bold font-size-h4">
                                    Already have an Account ?
                                    <a href="{{ route('login') }}" class="text-primary font-weight-bolder">Sign In</a>
                                </div>
                            </div>
                            <!--begin::Title-->

                            <!--begin::Form Group-->
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="form-group fv-plugins-icon-container">
                                        <label class="font-size-h6 font-weight-bolder text-dark">Name</label>
                                        <input type="text" class="form-control form-control-solid h-auto py-7 px-6 border-0 rounded-lg font-size-h6" name="name" placeholder="Name" value="{{ old('name') }}">
                                        <div class="fv-plugins-message-container"></div>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="form-group fv-plugins-icon-container">
                                        <label class="font-size-h6 font-weight-bolder text-dark">Email</label>
                                        <input type="email" class="form-control form-control-solid h-auto py-7 px-6 border-0 rounded-lg font-size-h6" name="email" placeholder="Email" value="{{ old('email') }}">
                                        <div class="fv-plugins-message-container"></div>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                        </div>
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="form-group fv-plugins-icon-container">
                                        <label class="font-size-h6 font-weight-bolder text-dark">Password</label>
                                        <input type="password" class="form-control form-control-solid h-auto py-7 px-6 border-0 rounded-lg font-size-h6" name="password" placeholder="password" value="{{ old('password') }}">
                                        <div class="fv-plugins-message-container"></div>
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                        </div>
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="form-group fv-plugins-icon-container">
                                        <label class="font-size-h6 font-weight-bolder text-dark">Confirm Password</label>
                                        <input type="password" class="form-control form-control-solid h-auto py-7 px-6 border-0 rounded-lg font-size-h6" name="password_confirmation" placeholder="Confirm Password" value="{{ old('password_confirmation') }}">
                                        <div class="fv-plugins-message-container"></div>
                                        @error('password_confirmation')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                        </div>
                        </div>
                        <!--end: Wizard Step 1-->

                        <!--begin: Wizard Step 2-->

                        <!--end: Wizard Step 5-->

                        <!--begin: Wizard Actions-->
                        <div class="d-flex justify-content-between pt-7">

                            <div>
                                <button type="submit" class="btn btn-primary font-weight-bolder font-size-h6 pl-8 pr-4 py-4 my-3" >
                                    Submit
                                    <span class="svg-icon svg-icon-md ml-2"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo8/dist/assets/media/svg/icons/Navigation/Right-2.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"></polygon>
        <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000) " x="7.5" y="7.5" width="2" height="9" rx="1"></rect>
        <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "></path>
    </g>
</svg><!--end::Svg Icon--></span>
                                </button>

                            </div>
                        </div>
                        <!--end: Wizard Actions-->
                        </form>
                    <!--end::Form-->
                </div>
                <!--end::Logo-->


            </div>
            <!--end::Signin-->
            <!--end::Wrapper-->
        </div>
        <!--begin::Content-->

        <!--begin::Aside-->
        <div class="login-aside order-1 order-lg-2 bgi-no-repeat bgi-position-x-right">
            <div class="login-conteiner bgi-no-repeat bgi-position-x-right bgi-position-y-bottom" style="background-image: url({{--{{ asset('theme/media/svg/illustrations/login-visual-4.svg') }}--}});">
                <!--begin::Aside title-->
                <h3 class="pt-lg-40 pl-lg-20 pb-lg-0 pl-10 py-20 m-0 d-flex justify-content-lg-start font-weight-boldest display5 display1-lg text-white">
                    Access Control Portal
                </h3>
                <!--end::Aside title-->
            </div>
        </div>
        <!--end::Aside-->
    </div>

@endsection
@section('scripts')


@endsection
