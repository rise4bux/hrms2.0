@extends('backend.layouts.auth_master')
@section('content')

    <div class="login login-4 wizard d-flex flex-column flex-lg-row flex-column-fluid">
        <!--begin::Content-->
        <div class="login-container order-2 order-lg-1 d-flex flex-center flex-row-fluid px-7 pt-lg-0 pb-lg-0 pt-4 pb-6 bg-white">
            <!--begin::Wrapper-->
            <!--begin::Signin-->
            <div class="login-content d-flex flex-column pt-lg-0 pt-12">
                <!--begin::Logo-->
                <a href="{{ route('login') }}" class="login-logo pb-xl-20 pb-15">
                    {{--<img src="{{ asset('theme/media/logos/logo-4.png') }}" class="max-h-70px" alt=""/>--}}
                    <img src="{{ asset('images/site_logo.png') }}" class="max-h-150px" alt="">
                </a>
                <div class="login-form">
                    <!--begin::Form-->

        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <!-- Email Address -->
            <div>
                <x-label for="email" :value="__('Email')" />

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <div class="flex items-center justify-end mt-4">
                <x-button>
                    {{ __('Email Password Reset Link') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
