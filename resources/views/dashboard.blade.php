@extends('backend.layouts.master')
@section('content')
    <!--begin::Content Wrapper-->
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                    {{--<h5 class="text-dark font-weight-bold my-2 mr-5">Reports</h5>--}}
                    <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="{{ route('dashboard') }}" class="text-muted">Dashboard</a>
                            </li>

                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Daterange-->
                    <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id=""
                       data-toggle="tooltip" title="" data-placement="left">
                        <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                        <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                    </a>
                    <!--end::Daterange-->
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <div class="content flex-column-fluid" id="kt_content">
            <!--begin::Dashboard-->
            <!--begin::Row-->
{{--            @if(!Auth::user()->hasRole('admin') )--}}
{{--                <div class="row">--}}
{{--                    <div class="col-xl-3">--}}
{{--                        <!--begin::Tiles Widget 21-->--}}
{{--                        <div class="card card-custom gutter-b">--}}
{{--                            <!--begin::Body-->--}}
{{--                            <div class="card-body d-flex flex-column p-0">--}}
{{--                                <!--begin::Stats-->--}}
{{--                                <div class="flex-grow-1 card-spacer pb-0">--}}
{{--                                    <div class="d-flex mb-4 justify-content-between   align-items-center">--}}
{{--                                        <i class="dashboard-icon fa fa-users"></i>--}}
{{--                                        <div class="font-weight-boldest font-size-h3 ">{{$today_guest_count}}</div>--}}
{{--                                    </div>--}}
{{--                                    <div class="text-muted font-weight-bold"> number of bookings for the current day--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <!--end::Stats-->--}}
{{--                                <!--begin::Chart-->--}}

{{--                                <!--end::Chart-->--}}
{{--                            </div>--}}
{{--                            <!--end::Body-->--}}
{{--                        </div>--}}
{{--                        <!--end::Tiles Widget 21-->--}}
{{--                    </div>--}}
{{--                    <div class="col-xl-3">--}}
{{--                        <!--begin::Tiles Widget 21-->--}}
{{--                        <div class="card card-custom gutter-b">--}}
{{--                            <!--begin::Body-->--}}
{{--                            <div class="card-body d-flex flex-column p-0">--}}
{{--                                <!--begin::Stats-->--}}
{{--                                <div class="flex-grow-1 card-spacer pb-0">--}}
{{--                                    <div class="d-flex mb-4 justify-content-between   align-items-center"><i--}}
{{--                                            class="dashboard-icon fa fa-thermometer-full"></i>--}}
{{--                                        <div--}}
{{--                                            class="font-weight-boldest font-size-h3 pt-2">{{round($avg_tempreture->temp?$avg_tempreture->temp:0,2)}}</div>--}}
{{--                                    </div>--}}
{{--                                    <div class="text-muted font-weight-bold">average temperature detected on all people--}}
{{--                                        for day--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <!--end::Stats-->--}}
{{--                                <!--begin::Chart-->--}}

{{--                                <!--end::Chart-->--}}
{{--                            </div>--}}
{{--                            <!--end::Body-->--}}
{{--                        </div>--}}
{{--                        <!--end::Tiles Widget 21-->--}}
{{--                    </div>--}}
{{--                    <div class="col-xl-3">--}}
{{--                        <!--begin::Tiles Widget 21-->--}}
{{--                        <div class="card card-custom gutter-b">--}}
{{--                            <!--begin::Body-->--}}
{{--                            <div class="card-body d-flex flex-column p-0">--}}
{{--                                <!--begin::Stats-->--}}
{{--                                <div class="flex-grow-1 card-spacer pb-0" id="accessedEmployeeCard">--}}
{{--                                    <div class="d-flex mb-4 justify-content-between   align-items-center"><i--}}
{{--                                            class="dashboard-icon fa fa-user-check"></i>--}}
{{--                                        <div--}}
{{--                                            class="font-weight-boldest font-size-h3 pt-2">{{$totalAccessedEmployeeCount}}</div>--}}
{{--                                    </div>--}}
{{--                                    <div class="text-muted font-weight-bold">People Accessed Today</div>--}}
{{--                                </div>--}}
{{--                                <!--end::Stats-->--}}
{{--                                <!--begin::Chart-->--}}

{{--                                <!--end::Chart-->--}}
{{--                            </div>--}}
{{--                            <!--end::Body-->--}}
{{--                        </div>--}}
{{--                        <!--end::Tiles Widget 21-->--}}
{{--                    </div>--}}
{{--                    <div class="col-xl-3">--}}
{{--                        <!--begin::Tiles Widget 21-->--}}
{{--                        <div class="card card-custom gutter-b">--}}
{{--                            <!--begin::Body-->--}}
{{--                            <div class="card-body d-flex flex-column p-0">--}}
{{--                                <!--begin::Stats-->--}}
{{--                                <div class="flex-grow-1 card-spacer pb-0" id="accessedGuest">--}}
{{--                                    <div class="d-flex mb-4 justify-content-between   align-items-center"><i--}}
{{--                                            class="dashboard-icon fa fa-check-double"></i>--}}
{{--                                        <div--}}
{{--                                            class="font-weight-boldest font-size-h3 pt-2">{{$totalGuestCountToday}}</div>--}}
{{--                                    </div>--}}
{{--                                    <div class="text-muted font-weight-bold">Total Guest Accessed Today</div>--}}
{{--                                </div>--}}
{{--                                <!--end::Stats-->--}}
{{--                                <!--begin::Chart-->--}}

{{--                                <!--end::Chart-->--}}
{{--                            </div>--}}
{{--                            <!--end::Body-->--}}
{{--                        </div>--}}
{{--                        <!--end::Tiles Widget 21-->--}}
{{--                    </div>--}}
{{--                    <div class="col-xl-3">--}}
{{--                        <!--begin::Tiles Widget 21-->--}}
{{--                        <div class="card card-custom gutter-b">--}}
{{--                            <!--begin::Body-->--}}
{{--                            <div class="card-body d-flex flex-column p-0">--}}
{{--                                <!--begin::Stats-->--}}
{{--                                <div class="flex-grow-1 card-spacer pb-0" id="accessed_employee">--}}
{{--                                    <div class="d-flex mb-4 justify-content-between   align-items-center"><i--}}
{{--                                            class="dashboard-icon fa fa-check-double"></i>--}}
{{--                                        <div class="font-weight-boldest font-size-h3 pt-2">{{$totalBookingToday}}</div>--}}
{{--                                    </div>--}}
{{--                                    <div class="text-muted font-weight-bold">Total Number of bookings today Click to see--}}
{{--                                        details--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <!--end::Stats-->--}}
{{--                                <!--begin::Chart-->--}}

{{--                                <!--end::Chart-->--}}
{{--                            </div>--}}
{{--                            <!--end::Body-->--}}
{{--                        </div>--}}
{{--                        <!--end::Tiles Widget 21-->--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div id="logModal" class="modal fade" role="dialog">--}}
{{--                    <div class="modal-dialog modal-lg">--}}

{{--                        <!-- Modal content-->--}}
{{--                        <div class="modal-content">--}}
{{--                            <div class="modal-header">--}}
{{--                                <h4 class="modal-title">Today's Booking Details</h4>--}}
{{--                                <button type="button" class="close" data-dismiss="modal">&times;</button>--}}

{{--                            </div>--}}
{{--                            <div class="modal-body">--}}
{{--                                <div class="row filters">--}}
{{--                                    <div class="col-xl-3 form-group">--}}
{{--                                        <label for="BuildingFilter">Building:</label><br>--}}
{{--                                        <select id="BuildingFilter" class="form-control">--}}
{{--                                            @foreach($buildings as $building)--}}
{{--                                                <option value="{{$building->id}}">{{$building->building_name}}</option>--}}
{{--                                            @endforeach--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-xl-3 form-group">--}}
{{--                                        <label for="floorFilter">Floor:</label><br>--}}
{{--                                        <select id="floorFilter" class="form-control">--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-xl-2">--}}
{{--                                        <label>Select Booking Type:</label><br>--}}
{{--                                        <label for=""><input type="checkbox" name="desk_type" value="desk"> Desk</label>--}}
{{--                                        <br>--}}
{{--                                        <label for=""><input type="checkbox" name="timeslot_type" value="timeslot">--}}
{{--                                            Time-slot</label>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-xl-4 form-group" id="locationBox" style="display:none">--}}
{{--                                        <label for="locationFilter">Location:</label>--}}
{{--                                        <select id="locationFilter" class="form-control"></select>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-xl-4 col-md-4 filters">--}}
{{--                                    <button class="btn btn-sm btn-primary" id="view-details">View Details</button>--}}
{{--                                </div>--}}
{{--                                <div class="main d-flex flex-column flex-row-fluid">--}}
{{--                                --}}{{--<div class="content flex-column-fluid" id="kt_content">--}}
{{--                                <!--begin::Card-->--}}
{{--                                    <div class="card card-custom">--}}
{{--                                        <div class="card-body" style=" max-height: 400px; overflow-y: scroll;">--}}

{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    --}}{{--</div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="modal-footer">--}}
{{--                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
{{--                            </div>--}}

{{--                        </div>--}}


{{--                    </div>--}}
{{--                </div>--}}
{{--                <div style="display: none" id="clonable">--}}
{{--                    <label class="label-light"></label>--}}
{{--                    <table class="table table-bordered">--}}
{{--                        <thead>--}}
{{--                        <tr>--}}
{{--                            <th>Name</th>--}}
{{--                            <th>TimeSlots</th>--}}
{{--                            <th>Desks</th>--}}
{{--                            <th>total</th>--}}
{{--                        </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}
{{--                        <tr class="tableTr" style="display:none"></tr>--}}
{{--                        </tbody>--}}
{{--                    </table>--}}
{{--                </div>--}}
{{--                <div style="display: none" id="bookingTableTimeslot">--}}
{{--                    <label class="label-light"></label>--}}
{{--                    <table class="table table-bordered">--}}
{{--                        <thead>--}}
{{--                        <tr>--}}
{{--                            <td>#</td>--}}
{{--                            <td>Name</td>--}}
{{--                            <td>Email</td>--}}
{{--                            <td>Start Time</td>--}}
{{--                            <td>End Time</td>--}}
{{--                        </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}
{{--                        </tbody>--}}
{{--                    </table>--}}
{{--                </div>--}}
{{--                <div style="display: none" id="bookingTableDesk">--}}
{{--                    <label class="label-light"></label>--}}
{{--                    <table class="table table-bordered">--}}
{{--                        <thead>--}}
{{--                        <tr>--}}
{{--                            <td>#</td>--}}
{{--                            <td>Name</td>--}}
{{--                            <td>Email</td>--}}
{{--                            <td>Desk Name</td>--}}
{{--                        </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}
{{--                        </tbody>--}}
{{--                    </table>--}}
{{--                </div>--}}
{{--                <div style="display: none" id="accesssedEmployeeTable">--}}
{{--                    <label class="label-light"></label>--}}
{{--                    <table class="table table-bordered">--}}
{{--                        <thead>--}}
{{--                        <tr>--}}
{{--                            <td>#</td>--}}
{{--                            <td>Name</td>--}}
{{--                            <td>Email</td>--}}
{{--                            <td>Accessed location</td>--}}
{{--                            <td>Entry time</td>--}}
{{--                        </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}
{{--                        </tbody>--}}
{{--                    </table>--}}
{{--                </div>--}}

{{--            @endif--}}
        </div>
        <option value="" id="hiddenOption" style="display: none"></option>
    </div>
@endsection
<!--begin::Content Wrapper-->
@section('scripts')
    <!--end::Global Theme Bundle-->
    {{-- <!--begin::Page Scripts(used by this page)-->
     <script src="{{ asset('theme/js/pages/widgets.js?v=7.0.3') }}"></script>--}}
    <!--begin::Page Vendors(used by this page)-->
    <script src="{{ asset('theme/js/pages/crud/forms/widgets/select2.js?v=7.0.3') }}"></script>
    <script src="{{ asset('theme/plugins/custom/fullcalendar/fullcalendar.bundle.js?v=7.0.3') }}"></script>

@endsection
<!--end::Page Vendors-->


