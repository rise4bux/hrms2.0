@section('title','Permissions')
@extends('backend.layouts.master')
@section('content')
    <!--begin::Content Wrapper-->
    <div class="main d-flex flex-column flex-row-fluid">
        <div class="content flex-column-fluid" id="kt_content">
            <div class="row">
                <div class="col-md-2">
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <h3 class="card-title">Roles</h3>
                        </div>
                        <div class="card-body px-5">
                            <div class="navi navi-hover navi-active navi-link-rounded navi-bold navi-icon-center navi-light-icon">
                                @foreach($allRolesIds as $row)
                                    <!--begin::Item-->
                                    <div class="navi-item my-2">
                                        <a href="{{ route('permission.index') .'/'.\Illuminate\Support\Facades\Crypt::encrypt($row->id) }}" class="navi-link {{ $row->id == $role ? 'active' : '' }}">
                                            <span class="navi-text font-weight-bolder font-size-lg">{{ ucwords($row->name) }}</span>
                                        </a>
                                    </div>
                                    <!--end::Item-->
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">Module wise Permission </h3>
                        </div>
                        <div class="card-body">
                            <div id="jstree">

                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-12">
                                    <button class="btn btn-success mr-2" id="add" >Add</button>
                                    <button class="btn btn-secondary">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Card-->
                </div>
            </div>
        </div>
        <!--end::Content-->
    </div>
    <!--begin::Content Wrapper-->
@endsection
@section('scripts')
    <script>
        $('#jstree').jstree({ 'core' : {
                'data' : {!! json_encode($treeData) !!},
                "themes" : {
                    "theme" : "default-dark",
                    "dots": true,
                    "variant" : "large",
                    "icons": false,
                    "responsive":true
                },
            },
            "plugins" : [ "checkbox" ],
            "checkbox": {
                "keep_selected_style": true
            }
        }).bind("loaded.jstree", function (event, data){
            $("#jstree").jstree("open_all");
        });
        $(document).on('click','#add',function (){
            var treeview = $("#jstree").jstree("get_selected",true);
            var parentIds = [];
            if(treeview) {
                for (var i=0;i < treeview.length;i++){
                    var arr = {
                        parents : treeview[i].parents,
                        parent : treeview[i].parent,
                        text :  treeview[i].text,
                    };
                    parentIds.push(arr);
                }
            }
            console.log(treeview,parentIds);
            $.ajax({
                url : "{!! route('permission.save-treeview') !!}",
                method:'POST',
                data : { roleId : "{!! \Illuminate\Support\Facades\Crypt::encrypt($role) !!}",treeview : parentIds },
                success:function (result){
                    if(result.status){
                        toastr.options = {
                            "progressBar": true,
                        };
                        toastr.success(result.message, "Success");
                    }
                }
            });
        })
    </script>
    {{--    @livewireScripts--}}
@endsection