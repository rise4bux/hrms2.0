<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>
        @yield('title') | {{ config('app.name') }}
    </title>

    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Global Theme Styles(used by all pages)-->
    {{--<link href="{{ asset('theme/plugins/custom/fullcalendar/fullcalendar.bundle.css?v=7.0.3') }}" rel="stylesheet" type="text/css" />--}}
    <!--end::Page Vendors Styles-->
    <!--begin::Global Theme Styles(used by all pages)-->
    <link href="{{ asset('theme/plugins/global/plugins.bundle.css?v=7.0.3') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/plugins/custom/prismjs/prismjs.bundle.css?v=7.0.3') }}" rel="stylesheet" type="text/css" />
    {{--<link href="{{ asset('theme/plugins/custom/fullcalendar/fullcalendar.bundle.css?v=7.0.3') }}" rel="stylesheet" type="text/css" />--}}
    <link href="{{ asset('theme/css/style.bundle.css?v=7.0.3') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Layout Themes-->
    <link rel="shortcut icon" href="{{ asset('images/intellyScan_logo_favicon.png') }}" type="image/x-icon"/>
    {{--<link href="{{ asset('theme/css/toastr.css')}}" rel="stylesheet" type="text/css"/>--}}
    <link href="https://codeseven.github.io/toastr/build/toastr.min.css" rel="stylesheet" type="text/css"/>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" rel="stylesheet">
    <!-- Js tree -->
{{--    <link rel="stylesheet" href="http://static.jstree.com/3.0.0-beta3/assets/dist/themes/default/style.min.css" />--}}
    <link rel="stylesheet" href="{{ asset('theme/plugins/custom/jstree/dist/themes/default/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('theme/plugins/custom/jstree/dist/themes/default-dark/style.min.css') }}" />
    @livewireStyles
    <style>
        .dashboard-icon{font-size:30px;color:#6E085C;}
        .fc-media-screen .fc-timegrid-event{
            position: relative !important;
        }
        #spinner-back, #spinner-front {
            position: fixed;
            width: 100%;
            transition: all 1s;
            opacity: 1;
        }
        #spinner-front {
            z-index: 999;
            color: #fff;
            height: 100%;

        }
        #spinner-back.show, #spinner-front.show {
            visibility: visible;
            opacity: 1;
        }

        </style>
</head>
<!-- end::Head -->
<body id="kt_body" class="header-fixed subheader-enabled page-loading">

<!--begin::Main-->
<div id="spinner-front" style="display: none">
    <img src="{{asset('theme/images/Spinner.gif')}}" style="position: absolute;
    top: 50%;
    left: 45%;"/>
</div>


    <!--begin::Page-->
    <div class="d-flex flex-row flex-column-fluid page">
        <!--begin::Wrapper-->
        <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
            <!--begin::Header Mobile-->
            @include('backend.layouts.header')
            <!--end::Header Mobile-->
            <!--begin::Header-->

            <!--end::Header Menu Wrapper-->
            <!--begin::Container-->
            <div class="d-flex flex-row flex-column-fluid container">
                @yield('content')
            </div>
            <!--end::Container-->
            <!--begin::Footer-->
            <div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
                <!--begin::Container-->
                @include('backend.layouts.footer')
                <!--end::Container-->
            </div>
            <!--end::Footer-->
        </div>
        <!--end::Wrapper-->
    </div>

    <!--end::Page-->

<!--end::Main-->
    {{--@include('backend.snippets.cart')--}}
    {{--@include('backend.snippets.quick_panel')--}}

<!--begin::Scrolltop-->
<div id="kt_scrolltop" class="scrolltop">
			<span class="svg-icon">
				<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Up-2.svg-->
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<polygon points="0 0 24 0 24 24 0 24" />
						<rect fill="#000000" opacity="0.3" x="11" y="10" width="2" height="10" rx="1" />
						<path d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z" fill="#000000" fill-rule="nonzero" />
					</g>
				</svg>
                <!--end::Svg Icon-->
			</span>
</div>
<!--end::Scrolltop-->
{{--@include('backend.snippets.sticky_tools')--}}

<script>var HOST_URL = "https://keenthemes.com/metronic/tools/preview";</script>
<!--begin::Global Config(global config for global JS scripts)-->
<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#8950FC", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#6993FF", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#EEE5FF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#E1E9FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
<!--end::Global Config-->

<!--begin::Global Theme Bundle(used by all pages)-->
<script src="{{ asset('theme/plugins/global/plugins.bundle.js?v=7.0.3') }}"></script>
<script src="{{ asset('theme/plugins/custom/prismjs/prismjs.bundle.js?v=7.0.3') }}"></script>
<script src="{{ asset('theme/plugins/custom/prismjs/prismjs.bundle.js?v=7.0.3') }}"></script>
{{--<script src="{{ asset('theme/plugins/custom/fullcalendar/fullcalendar.bundle.js?v=7.0.3') }}"></script>--}}
{{--<script src="{{ asset('theme/js/pages/features/calendar/basic.js') }}"></script>--}}
<script src="{{ asset('theme/js/scripts.bundle.js?v=7.0.3') }}"></script>

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<script src="{{ asset('theme/js/toastr.js') }}" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<!-- Js tree -->
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>--}}
<script src="{{ asset('theme/plugins/custom/jstree/dist/jstree.min.js') }}"></script>
<script>
    var AjaxUrl = '{{URL::to('/')}}';
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
    // var loading = new KTDialog({
    //     'type': 'loader',
    //     'placement': 'top center',
    //     'message': 'Loading ...'
    // });
</script>

<script>

    @if(session()->has('success'))
        toastr.options = {
        "progressBar": true,
    };
    toastr.success("{{ session()->get('success') }}", "Success");
    @php
        session()->forget('success');
    @endphp
    @endif

    @if(session()->has('failure'))
        toastr.options = {
        "progressBar": true,
    };
    toastr.error("{{ session()->get('failure') }}", "Error");
    @php
        session()->forget('failure');
    @endphp
    @endif

    @if ($errors->any())
    @foreach ($errors->all() as $error)
        toastr.options = {
        "progressBar": true,
    };
    toastr.error("{{ $error }}", "Error");
    @endforeach
    @endif

    {{--@if(session()->has('success'))--}}
    {{--toastr.success("{{ session()->get('success') }}")--}}
    {{--@endif--}}
    $('table').dataTable({
        'order':[]
    });

</script>
<script>
    $(document).on('click','.delete',function (){
        var t = this;
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url : $(t).data('url'),
                    method : 'DELETE',
                    success:function (result){
                        if(result.status){
                            toastr.success(result.message, "Success");
                            $(t).closest('tr').remove();
                        }else{
                            toastr.error(result.message, "Error");
                        }
                    }
                });
            }
        })

    });
</script>
@yield('scripts')
@livewireScripts
</body>
</html>
