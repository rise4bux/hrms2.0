<div class="container">
    <!--begin::Header Menu-->
    <div id="kt_header_menu" class="header-menu header-menu-left header-menu-mobile header-menu-layout-default header-menu-root-arrow">
        <!--begin::Header Nav-->


        <ul class="menu-nav">
            @role('admin')
            <li class="menu-item menu-item-submenu menu-item-rel {{ request()->segment(1) == 'role-management' ? 'menu-item-active' : '' }}" data-menu-toggle="click" aria-haspopup="true">
                    <a href="javascript:;" class="menu-link menu-toggle">
                        <span class="menu-text">Role Management</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu menu-submenu-classic menu-submenu-left" data-menu-toggle="hover" aria-haspopup="true">
                        <ul class="menu-subnav">
                            <li class="menu-item {{ request()->segment(2) == 'roles' ? 'menu-item-open  menu-item-active' : '' }}" aria-haspopup="true">
                                <a href="{{route('role.index')}}"  class="menu-link ">
                                    <span class="menu-text">Roles</span>
                                </a>
                            </li>
                            <li class="menu-item {{ request()->segment(2) == 'permission' ? 'menu-item-open  menu-item-active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('permission.index') }}" class="menu-link">
                                    <span class="menu-text">Permissions</span>
                                    <span class="menu-desc"></span>
                                </a>
                            </li>
                            <li class="menu-item {{ request()->segment(2) == 'module-master'  ? 'menu-item-open  menu-item-active' : '' }}" >
                                <a href="{{route('modulemaster.index')}}" class="menu-link ">
                                    <span class="menu-text">Modules</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="menu-item {{ request()->segment(1) == 'employee'  ? 'menu-item-open  menu-item-active' : '' }}" >
                    <a href="{{route('employee.index')}}" class="menu-link ">
                        <span class="menu-text">Employee</span>
                    </a>
                </li>
            @endrole
            @livewire('create-parent-links')
        </ul>

        <!--end::Header Nav-->

    </div>
    <!--end::Header Menu-->
</div>
