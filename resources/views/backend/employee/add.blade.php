
@section('title','Add Employee')
@extends('backend.layouts.master')
@section('content')

    <!--begin::Content Wrapper-->
    <div class="main d-flex flex-column flex-row-fluid">
        <div class="content flex-column-fluid" id="kt_content">
            <div class="row">
                <div class="col-md-12">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">{{ isset($employeeData) && !empty($employeeData) ? 'Update Employee' : 'Add Employee' }}</h3>
                        </div>
                        <!--begin::Form-->
                        <form action="{{ (isset($employeeData) && !empty($employeeData)) ? route("employee.store").'/'.$id : route("employee.store") }}" method="POST" autocomplete="off">
                            @csrf
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="name" class="col-2 col-form-label">Name</label>
                                    <div class="col-10">
                                        <input class="form-control" type="text" id="name" name="name" value="{{ (isset($employeeData) && !empty($employeeData)) ? $employeeData->name : '' }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-2 col-form-label">Email</label>
                                    <div class="col-10">
                                        <input class="form-control" type="text" id="email" name="email" value="{{ (isset($employeeData) && !empty($employeeData)) ? $employeeData->email : '' }}">
                                    </div>
                                </div>
                                @if(isset($employeeData) && empty($employeeData))
                                    <div class="form-group row">
                                        <label for="password" class="col-2 col-form-label">Password</label>
                                        <div class="col-10">
                                            <input class="form-control" type="password" id="password" name="password" autocomplete="new-password" >
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group row">
                                    <label for="department_id" class="col-2 col-form-label">Department</label>
                                    <div class="col-10">
                                        <select class="form-control" name="department_id">
                                            @foreach($departmentData as $row)
                                                <option value="{{ $row->name }}" {{ isset($employeeData->department_id) && $employeeData->department_id == $row->department_id ? 'selected' : '' }} >{{ ucfirst($row->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="reporting_head" class="col-2 col-form-label">Reporting Head</label>
                                    <div class="col-10">
                                        <input class="form-control" type="text" name="reporting_head"  >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="password" class="col-2 col-form-label">Select Role</label>
                                    <div class="col-10">
                                        <select class="form-control" name="role">
                                            @foreach($roles as $row)
                                                <option value="{{ $row->name }}" {{ isset($employeeData->roles) && $employeeData->roles->first()->id == $row->id ? 'selected' : '' }} >{{ ucfirst($row->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-success mr-2">Submit</button>
                                        <button type="reset" class="btn btn-secondary">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Card-->
                </div>
            </div>
        </div>
        <!--end::Content-->
    </div>
    <!--begin::Content Wrapper-->

@endsection
