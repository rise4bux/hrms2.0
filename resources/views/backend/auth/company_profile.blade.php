@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5">  Company Profile</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="{{ route('view.profile') }}" class="text-muted">Profile</a>
                            </li>

                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Daterange-->
                    <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="" data-toggle="tooltip" title="" data-placement="left">
                        <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                        <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                    </a>
                    <!--end::Daterange-->
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <div class="d-flex flex-row">
                <!--begin::Aside-->
                @include('backend.snippets.user_profile')
                <!--end::Aside-->
                <!--begin::Content-->
                <div class="flex-row-fluid ml-lg-8">
                    <!--begin::Row-->
                    <div class="row">
                            <div class="col-lg-8">
                            <!--begin::List Widget 14-->
                            <div class="card card-custom card-stretch gutter-b">
                                <!--begin::Header-->
                                <div class="card-header border-0">
                                    <h3 class="card-title font-weight-bolder text-dark">Profile Overview</h3>

                                    {{--<div class="card-toolbar">
                                        <div class="dropdown dropdown-inline">
                                            <a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="ki ki-bold-more-ver"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
                                                <!--begin::Navigation-->
                                                <ul class="navi navi-hover">
                                                    <li class="navi-header font-weight-bold py-4">
                                                        <span class="font-size-lg">Choose Label:</span>
                                                        <i class="flaticon2-information icon-md text-muted" data-toggle="tooltip" data-placement="right" title="Click to learn more..."></i>
                                                    </li>
                                                    <li class="navi-separator mb-3 opacity-70"></li>
                                                    <li class="navi-item">
                                                        <a href="#" class="navi-link">
																				<span class="navi-text">
																					<span class="label label-xl label-inline label-light-success">Customer</span>
																				</span>
                                                        </a>
                                                    </li>
                                                    <li class="navi-item">
                                                        <a href="#" class="navi-link">
																				<span class="navi-text">
																					<span class="label label-xl label-inline label-light-danger">Partner</span>
																				</span>
                                                        </a>
                                                    </li>
                                                    <li class="navi-item">
                                                        <a href="#" class="navi-link">
																				<span class="navi-text">
																					<span class="label label-xl label-inline label-light-warning">Suplier</span>
																				</span>
                                                        </a>
                                                    </li>
                                                    <li class="navi-item">
                                                        <a href="#" class="navi-link">
																				<span class="navi-text">
																					<span class="label label-xl label-inline label-light-primary">Member</span>
																				</span>
                                                        </a>
                                                    </li>
                                                    <li class="navi-item">
                                                        <a href="#" class="navi-link">
																				<span class="navi-text">
																					<span class="label label-xl label-inline label-light-dark">Staff</span>
																				</span>
                                                        </a>
                                                    </li>
                                                    <li class="navi-separator mt-3 opacity-70"></li>
                                                    <li class="navi-footer py-4">
                                                        <a class="btn btn-clean font-weight-bold btn-sm" href="#">
                                                            <i class="ki ki-plus icon-sm"></i>Add new</a>
                                                    </li>
                                                </ul>
                                                <!--end::Navigation-->
                                            </div>
                                        </div>
                                    </div>--}}
                                </div>
                                <!--end::Header-->
                                <!--begin::Body-->
                                <div class="card-body pt-2">
                                    @if(!empty($userCompany))
                                        <div class="row mt-4">
                                            <div class="col-lg">Company ID :</div>
                                            <div class="col-lg"> @if (isset($userCompany) )  {{$userCompany->id}} @endif</div>
                                        </div>
                                        <div class="row mt-4">
                                            <div class="col-lg">Tracking services :</div>
                                            <div class="col-lg"> @if (isset($userCompany) && $userCompany->is_tracking_enabled==1) <span class="text-success">Available</span> @else <span class="text-warning">Not available</span> @endif</div>
                                        </div>
                                        <div class="row mt-4">
                                            <div class="col-lg">Booking services :</div>
                                            <div class="col-lg"> @if (isset($userCompany) && $userCompany->is_booking_enabled==1) <span class="text-success">Available</span> @else <span class="text-warning">Not available</span> @endif</div>
                                        </div>
                                        <div class="row mt-4">
                                            <div class="col-lg">Employee Limit :</div>
                                            <div class="col-lg"> @if (isset($userCompany) && $userCompany->employee_limit!=null) <span class="text-success">{{$userCompany->employee_limit}}</span> @else <span class="text-warning">Not Added</span> @endif</div>
                                        </div>
                                    @endif


                                    <!--begin::Item-->
                                   {{-- <div class="d-flex flex-wrap align-items-center mb-10">
                                        <!--begin::Symbol-->
                                        <div class="symbol symbol-60 symbol-2by3 flex-shrink-0 mr-4">
                                            <div class="symbol-label" style="background-image: url('assets/media/stock-600x400/img-17.jpg')"></div>
                                        </div>
                                        <!--end::Symbol-->
                                        <!--begin::Title-->
                                        <div class="d-flex flex-column flex-grow-1 my-lg-0 my-2 pr-3">
                                            <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary font-size-lg">Cup &amp; Green</a>
                                            <span class="text-muted font-weight-bold font-size-sm my-1">Local, clean &amp; environmental</span>
                                            <span class="text-muted font-weight-bold font-size-sm">Created by:
																<span class="text-primary font-weight-bold">CoreAd</span></span>
                                        </div>
                                        <!--end::Title-->
                                        <!--begin::Info-->
                                        <div class="d-flex align-items-center py-lg-0 py-2">
                                            <div class="d-flex flex-column text-right">
                                                <span class="text-dark-75 font-weight-bolder font-size-h4">24,900</span>
                                                <span class="text-muted font-size-sm font-weight-bolder">votes</span>
                                            </div>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::Item-->
                                    <!--begin: Item-->
                                    <div class="d-flex flex-wrap align-items-center mb-10">
                                        <!--begin::Symbol-->
                                        <div class="symbol symbol-60 symbol-2by3 flex-shrink-0 mr-4">
                                            <div class="symbol-label" style="background-image: url('assets/media/stock-600x400/img-10.jpg')"></div>
                                        </div>
                                        <!--end::Symbol-->
                                        <!--begin::Title-->
                                        <div class="d-flex flex-column flex-grow-1 my-lg-0 my-2 pr-3">
                                            <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary font-size-lg">Yellow Background</a>
                                            <span class="text-muted font-weight-bold font-size-sm my-1">Strong abstract concept</span>
                                            <span class="text-muted font-weight-bold font-size-sm">Created by:
																<span class="text-primary font-weight-bold">KeenThemes</span></span>
                                        </div>
                                        <!--end::Title-->
                                        <!--begin::Info-->
                                        <div class="d-flex align-items-center py-lg-0 py-2">
                                            <div class="d-flex flex-column text-right">
                                                <span class="text-dark-75 font-weight-bolder font-size-h4">70,380</span>
                                                <span class="text-muted font-weight-bolder font-size-sm">votes</span>
                                            </div>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end: Item-->
                                    <!--begin::Item-->
                                    <div class="d-flex flex-wrap align-items-center mb-10">
                                        <!--begin::Symbol-->
                                        <div class="symbol symbol-60 symbol-2by3 flex-shrink-0 mr-4">
                                            <div class="symbol-label" style="background-image: url('assets/media/stock-600x400/img-1.jpg')"></div>
                                        </div>
                                        <!--end::Symbol-->
                                        <!--begin::Title-->
                                        <div class="d-flex flex-column flex-grow-1 pr-3">
                                            <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary font-size-lg">Nike &amp; Blue</a>
                                            <span class="text-muted font-weight-bold font-size-sm my-1">Footwear overalls</span>
                                            <span class="text-muted font-weight-bold font-size-sm">Created by:
																<span class="text-primary font-weight-bold">Invision Inc.</span></span>
                                        </div>
                                        <!--end::Title-->
                                        <!--begin::Info-->
                                        <div class="d-flex align-items-center py-lg-0 py-2">
                                            <div class="d-flex flex-column text-right">
                                                <span class="text-dark-75 font-size-h4 font-weight-bolder">7,200</span>
                                                <span class="text-muted font-size-sm font-weight-bolder">votes</span>
                                            </div>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::Item-->
                                    <!--begin::Item-->
                                    <div class="d-flex flex-wrap align-items-center mb-10">
                                        <!--begin::Symbol-->
                                        <div class="symbol symbol-60 symbol-2by3 flex-shrink-0 mr-4">
                                            <div class="symbol-label" style="background-image: url('assets/media/stock-600x400/img-9.jpg')"></div>
                                        </div>
                                        <!--end::Symbol-->
                                        <!--begin::Title-->
                                        <div class="d-flex flex-column flex-grow-1 my-lg-0 my-2 pr-3">
                                            <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary font-size-lg">Desserts platter</a>
                                            <span class="text-muted font-size-sm font-weight-bold my-1">Food trends &amp; reviews</span>
                                            <span class="text-muted font-size-sm font-weight-bold">Created by:
																<span class="text-primary font-weight-bold">Figma Studio</span></span>
                                        </div>
                                        <!--end::Title-->
                                        <!--begin::Info-->
                                        <div class="d-flex align-items-center py-lg-0 py-2">
                                            <div class="d-flex flex-column text-right">
                                                <span class="text-dark-75 font-size-h4 font-weight-bolder">36,450</span>
                                                <span class="text-muted font-size-sm font-weight-bolder">votes</span>
                                            </div>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::Item-->
                                    <!--begin::Item-->
                                    <div class="d-flex flex-wrap align-items-center">
                                        <!--begin::Symbol-->
                                        <div class="symbol symbol-60 symbol-2by3 flex-shrink-0 mr-4">
                                            <div class="symbol-label" style="background-image: url('assets/media/stock-600x400/img-12.jpg')"></div>
                                        </div>
                                        <!--end::Symbol-->
                                        <!--begin::Title-->
                                        <div class="d-flex flex-column flex-grow-1 my-lg-0 my-2 pr-3">
                                            <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary font-size-lg">Cup &amp; Green</a>
                                            <span class="text-muted font-weight-bold font-size-sm my-1">Local, clean &amp; environmental</span>
                                            <span class="text-muted font-weight-bold font-size-sm">Created by:
																<span class="text-primary font-weight-bold">CoreAd</span></span>
                                        </div>
                                        <!--end::Title-->
                                        <!--begin::Info-->
                                        <div class="d-flex align-items-center py-lg-0 py-2">
                                            <div class="d-flex flex-column text-right">
                                                <span class="text-dark-75 font-weight-bolder font-size-h4">23,900</span>
                                                <span class="text-muted font-size-sm font-weight-bolder">votes</span>
                                            </div>
                                        </div>
                                        <!--end::Info-->
                                    </div>--}}
                                    <!--end::Item-->
                                </div>
                                <!--end::Body-->
                            </div>
                            <!--end::List Widget 14-->
                        </div>
                        {{--<div class="col-lg-6">--}}
                            {{--<!--begin::List Widget 10-->--}}
                            {{--<div class="card card-custom card-stretch gutter-b">--}}
                                {{--<!--begin::Header-->--}}
                                {{--<div class="card-header border-0">--}}
                                    {{--<h3 class="card-title font-weight-bolder text-dark">Profile Overview</h3>--}}
                                    {{--<div class="card-toolbar">--}}
                                        {{--<div class="dropdown dropdown-inline">--}}
                                            {{--<a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                                                {{--<i class="ki ki-bold-more-ver"></i>--}}
                                            {{--</a>--}}
                                            {{--<div class="dropdown-menu dropdown-menu-md dropdown-menu-right">--}}
                                                {{--<!--begin::Naviigation-->--}}
                                                {{--<ul class="navi">--}}
                                                    {{--<li class="navi-header font-weight-bold py-5">--}}
                                                        {{--<span class="font-size-lg">Add New:</span>--}}
                                                        {{--<i class="flaticon2-information icon-md text-muted" data-toggle="tooltip" data-placement="right" title="Click to learn more..."></i>--}}
                                                    {{--</li>--}}
                                                    {{--<li class="navi-separator mb-3 opacity-70"></li>--}}
                                                    {{--<li class="navi-item">--}}
                                                        {{--<a href="#" class="navi-link">--}}
																				{{--<span class="navi-icon">--}}
																					{{--<i class="flaticon2-shopping-cart-1"></i>--}}
																				{{--</span>--}}
                                                            {{--<span class="navi-text">Order</span>--}}
                                                        {{--</a>--}}
                                                    {{--</li>--}}
                                                    {{--<li class="navi-item">--}}
                                                        {{--<a href="#" class="navi-link">--}}
																				{{--<span class="navi-icon">--}}
																					{{--<i class="navi-icon flaticon2-calendar-8"></i>--}}
																				{{--</span>--}}
                                                            {{--<span class="navi-text">Members</span>--}}
                                                            {{--<span class="navi-label">--}}
																					{{--<span class="label label-light-danger label-rounded font-weight-bold">3</span>--}}
																				{{--</span>--}}
                                                        {{--</a>--}}
                                                    {{--</li>--}}
                                                    {{--<li class="navi-item">--}}
                                                        {{--<a href="#" class="navi-link">--}}
																				{{--<span class="navi-icon">--}}
																					{{--<i class="navi-icon flaticon2-telegram-logo"></i>--}}
																				{{--</span>--}}
                                                            {{--<span class="navi-text">Project</span>--}}
                                                        {{--</a>--}}
                                                    {{--</li>--}}
                                                    {{--<li class="navi-item">--}}
                                                        {{--<a href="#" class="navi-link">--}}
																				{{--<span class="navi-icon">--}}
																					{{--<i class="navi-icon flaticon2-new-email"></i>--}}
																				{{--</span>--}}
                                                            {{--<span class="navi-text">Record</span>--}}
                                                            {{--<span class="navi-label">--}}
																					{{--<span class="label label-light-success label-rounded font-weight-bold">5</span>--}}
																				{{--</span>--}}
                                                        {{--</a>--}}
                                                    {{--</li>--}}
                                                    {{--<li class="navi-separator mt-3 opacity-70"></li>--}}
                                                    {{--<li class="navi-footer pt-5 pb-4">--}}
                                                        {{--<a class="btn btn-light-primary font-weight-bolder btn-sm" href="#">More options</a>--}}
                                                        {{--<a class="btn btn-clean font-weight-bold btn-sm d-none" href="#" data-toggle="tooltip" data-placement="right" title="Click to learn more...">Learn more</a>--}}
                                                    {{--</li>--}}
                                                {{--</ul>--}}
                                                {{--<!--end::Naviigation-->--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<!--end::Header-->--}}
                                {{--<!--begin::Body-->--}}
                                {{--<div class="card-body pt-0">--}}
                                    {{--<!--begin::Item-->--}}
                                    {{--<div class="mb-6">--}}
                                        {{--<!--begin::Content-->--}}
                                        {{--<div class="d-flex align-items-center flex-grow-1">--}}
                                            {{--<!--begin::Checkbox-->--}}
                                            {{--<label class="checkbox checkbox-lg checkbox-lg checkbox-single flex-shrink-0 mr-4">--}}
                                                {{--<input type="checkbox" value="1" />--}}
                                                {{--<span></span>--}}
                                            {{--</label>--}}
                                            {{--<!--end::Checkbox-->--}}
                                            {{--<!--begin::Section-->--}}
                                            {{--<div class="d-flex flex-wrap align-items-center justify-content-between w-100">--}}
                                                {{--<!--begin::Info-->--}}
                                                {{--<div class="d-flex flex-column align-items-cente py-2 w-75">--}}
                                                    {{--<!--begin::Title-->--}}
                                                    {{--<a href="#" class="text-dark-75 font-weight-bold text-hover-primary font-size-lg mb-1">Daily Standup Meeting</a>--}}
                                                    {{--<!--end::Title-->--}}
                                                    {{--<!--begin::Data-->--}}
                                                    {{--<span class="text-muted font-weight-bold">Due in 2 Days</span>--}}
                                                    {{--<!--end::Data-->--}}
                                                {{--</div>--}}
                                                {{--<!--end::Info-->--}}
                                                {{--<!--begin::Label-->--}}
                                                {{--<span class="label label-lg label-light-primary label-inline font-weight-bold py-4">Approved</span>--}}
                                                {{--<!--end::Label-->--}}
                                            {{--</div>--}}
                                            {{--<!--end::Section-->--}}
                                        {{--</div>--}}
                                        {{--<!--end::Content-->--}}
                                    {{--</div>--}}
                                    {{--<!--end::Item-->--}}
                                    {{--<!--begin::Item-->--}}
                                    {{--<div class="mb-6">--}}
                                        {{--<!--begin::Content-->--}}
                                        {{--<div class="d-flex align-items-center flex-grow-1">--}}
                                            {{--<!--begin::Checkbox-->--}}
                                            {{--<label class="checkbox checkbox-lg checkbox-lg checkbox-single flex-shrink-0 mr-4">--}}
                                                {{--<input type="checkbox" value="1" />--}}
                                                {{--<span></span>--}}
                                            {{--</label>--}}
                                            {{--<!--end::Checkbox-->--}}
                                            {{--<!--begin::Section-->--}}
                                            {{--<div class="d-flex flex-wrap align-items-center justify-content-between w-100">--}}
                                                {{--<!--begin::Info-->--}}
                                                {{--<div class="d-flex flex-column align-items-cente py-2 w-75">--}}
                                                    {{--<!--begin::Title-->--}}
                                                    {{--<a href="#" class="text-dark-75 font-weight-bold text-hover-primary font-size-lg mb-1">Group Town Hall Meet-up with showcase</a>--}}
                                                    {{--<!--end::Title-->--}}
                                                    {{--<!--begin::Data-->--}}
                                                    {{--<span class="text-muted font-weight-bold">Due in 2 Days</span>--}}
                                                    {{--<!--end::Data-->--}}
                                                {{--</div>--}}
                                                {{--<!--end::Info-->--}}
                                                {{--<!--begin::Label-->--}}
                                                {{--<span class="label label-lg label-light-warning label-inline font-weight-bold py-4">In Progress</span>--}}
                                                {{--<!--end::Label-->--}}
                                            {{--</div>--}}
                                            {{--<!--end::Section-->--}}
                                        {{--</div>--}}
                                        {{--<!--end::Content-->--}}
                                    {{--</div>--}}
                                    {{--<!--end::Item-->--}}
                                    {{--<!--begin::Item-->--}}
                                    {{--<div class="mb-6">--}}
                                        {{--<!--begin::Content-->--}}
                                        {{--<div class="d-flex align-items-center flex-grow-1">--}}
                                            {{--<!--begin::Checkbox-->--}}
                                            {{--<label class="checkbox checkbox-lg checkbox-lg checkbox-single flex-shrink-0 mr-4">--}}
                                                {{--<input type="checkbox" value="1" />--}}
                                                {{--<span></span>--}}
                                            {{--</label>--}}
                                            {{--<!--end::Checkbox-->--}}
                                            {{--<!--begin::Section-->--}}
                                            {{--<div class="d-flex flex-wrap align-items-center justify-content-between w-100">--}}
                                                {{--<!--begin::Info-->--}}
                                                {{--<div class="d-flex flex-column align-items-cente py-2 w-75">--}}
                                                    {{--<!--begin::Title-->--}}
                                                    {{--<a href="#" class="text-dark-75 font-weight-bold text-hover-primary font-size-lg mb-1">Next sprint planning and estimations</a>--}}
                                                    {{--<!--end::Title-->--}}
                                                    {{--<!--begin::Data-->--}}
                                                    {{--<span class="text-muted font-weight-bold">Due in 2 Days</span>--}}
                                                    {{--<!--end::Data-->--}}
                                                {{--</div>--}}
                                                {{--<!--end::Info-->--}}
                                                {{--<!--begin::Label-->--}}
                                                {{--<span class="label label-lg label-light-success label-inline font-weight-bold py-4">Success</span>--}}
                                                {{--<!--end::Label-->--}}
                                            {{--</div>--}}
                                            {{--<!--end::Section-->--}}
                                        {{--</div>--}}
                                        {{--<!--end::Content-->--}}
                                    {{--</div>--}}
                                    {{--<!--end::Item-->--}}
                                    {{--<!--begin::Item-->--}}
                                    {{--<div class="mb-6">--}}
                                        {{--<!--begin::Content-->--}}
                                        {{--<div class="d-flex align-items-center flex-grow-1">--}}
                                            {{--<!--begin::Checkbox-->--}}
                                            {{--<label class="checkbox checkbox-lg checkbox-lg checkbox-single flex-shrink-0 mr-4">--}}
                                                {{--<input type="checkbox" value="1" />--}}
                                                {{--<span></span>--}}
                                            {{--</label>--}}
                                            {{--<!--end::Checkbox-->--}}
                                            {{--<!--begin::Section-->--}}
                                            {{--<div class="d-flex flex-wrap align-items-center justify-content-between w-100">--}}
                                                {{--<!--begin::Info-->--}}
                                                {{--<div class="d-flex flex-column align-items-cente py-2 w-75">--}}
                                                    {{--<!--begin::Title-->--}}
                                                    {{--<a href="#" class="text-dark-75 font-weight-bold text-hover-primary font-size-lg mb-1">Sprint delivery and project deployment</a>--}}
                                                    {{--<!--end::Title-->--}}
                                                    {{--<!--begin::Data-->--}}
                                                    {{--<span class="text-muted font-weight-bold">Due in 2 Days</span>--}}
                                                    {{--<!--end::Data-->--}}
                                                {{--</div>--}}
                                                {{--<!--end::Info-->--}}
                                                {{--<!--begin::Label-->--}}
                                                {{--<span class="label label-lg label-light-danger label-inline font-weight-bold py-4">Rejected</span>--}}
                                                {{--<!--end::Label-->--}}
                                            {{--</div>--}}
                                            {{--<!--end::Section-->--}}
                                        {{--</div>--}}
                                        {{--<!--end::Content-->--}}
                                    {{--</div>--}}
                                    {{--<!--end: Item-->--}}
                                    {{--<!--begin: Item-->--}}
                                    {{--<div class="">--}}
                                        {{--<!--begin::Content-->--}}
                                        {{--<div class="d-flex align-items-center flex-grow-1">--}}
                                            {{--<!--begin::Checkbox-->--}}
                                            {{--<label class="checkbox checkbox-lg checkbox-lg checkbox-single flex-shrink-0 mr-4">--}}
                                                {{--<input type="checkbox" value="1" />--}}
                                                {{--<span></span>--}}
                                            {{--</label>--}}
                                            {{--<!--end::Checkbox-->--}}
                                            {{--<!--begin::Section-->--}}
                                            {{--<div class="d-flex flex-wrap align-items-center justify-content-between w-100">--}}
                                                {{--<!--begin::Info-->--}}
                                                {{--<div class="d-flex flex-column align-items-cente py-2 w-75">--}}
                                                    {{--<!--begin::Title-->--}}
                                                    {{--<a href="#" class="text-dark-75 font-weight-bold text-hover-primary font-size-lg mb-1">Data analytics research showcase</a>--}}
                                                    {{--<!--end::Title-->--}}
                                                    {{--<!--begin::Data-->--}}
                                                    {{--<span class="text-muted font-weight-bold">Due in 2 Days</span>--}}
                                                    {{--<!--end::Data-->--}}
                                                {{--</div>--}}
                                                {{--<!--end::Info-->--}}
                                                {{--<!--begin::Label-->--}}
                                                {{--<span class="label label-lg label-light-warning label-inline font-weight-bold py-4">In Progress</span>--}}
                                                {{--<!--end::Label-->--}}
                                            {{--</div>--}}
                                            {{--<!--end::Section-->--}}
                                        {{--</div>--}}
                                        {{--<!--end::Content-->--}}
                                    {{--</div>--}}
                                    {{--<!--end: Item-->--}}
                                {{--</div>--}}
                                {{--<!--end: Card Body-->--}}
                            {{--</div>--}}
                            {{--<!--end: Card-->--}}
                            {{--<!--end: List Widget 10-->--}}
                        {{--</div>--}}
                    </div>
                    <!--end::Row-->
                    <!--begin::Advance Table: Widget 7-->

                    <!--end::Advance Table Widget 7-->
                </div>
                <!--end::Content-->
            </div>

        </div>
        <!--end::Content-->
    </div>

@endsection

@section('scripts')


@endsection



