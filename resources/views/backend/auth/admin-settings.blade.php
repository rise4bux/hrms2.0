@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5"> User Profile</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="{{ route('view.profile') }}" class="text-muted">Profile</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('profile.settings') }}" class="text-muted">Settings</a>
                            </li>

                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Daterange-->
                    <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="" data-toggle="tooltip" title="" data-placement="left">
                        <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                        <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                    </a>
                    <!--end::Daterange-->
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <div class="d-flex flex-row">
                <!--begin::Aside-->

            @include('backend.snippets.user_profile')
            <!--end::Aside-->
                <!--begin::Content-->
                <div class="flex-row-fluid ml-lg-8">
                    <!--begin::Content-->
                    <div class="flex-row-fluid ml-lg-8">
                        <!--begin::Card-->
                        <div class="card card-custom card-stretch">
                            <!--begin::Header-->
                            <div class="card-header py-3">
                                <div class="card-title align-items-start flex-column">
                                    <h3 class="card-label font-weight-bolder text-dark">Settings</h3>
                                    <span class="text-muted font-weight-bold font-size-sm mt-1">Update Settings</span>
                                </div>
                                <div class="card-toolbar">
                                    <button type="button" class="btn btn-success mr-2" id="saveButton">Save Changes</button>
                                    <button type="reset" class="btn btn-secondary" onclick="return window.location.href='{{ route('view.profile') }}'">Cancel</button>
                                </div>
                            </div>
                            <!--end::Header-->
                            <!--begin::Form-->
                            <form id="ProfileForm" class="form"
                                  method="POST" enctype="multipart/form-data"
                                  action="{{ route('update-admin.settings') }}">
                            @csrf
                            <!--begin::Body-->
                                <div class="card-body settingContainer">
                                    <div class="form-group row">
                                        <button class="btn btn-sm btn-primary right add-more-button" type="button">Add more</button>
                                    </div>
                                    <!--begin::Alert-->
                                    <div class="form-group row admin-settings count" style="display: none">
                                            <div class="col-xl-4 col-lg-3"><input class="form-control form-control-lg form-control-solid" type="text"  placeholder="Enter key" id="key"></div>
                                            <div class="col-xl-8" style="display: flex"><input id="value" type="text" class="form-control form-control-lg form-control-solid" placeholder="Enter Value"/><a href="javascript:void(0)" class="trash-button btn btn-sm btn-default  btn-hover-danger btn-icon mt-2 ml-2">
                                                    <i class="la la-trash" style="font-size: 30px;color: red" title="delete"></i></a></div>
                                    </div>
                                   @if(isset($settings))
                                        @foreach($settings as $setting)
                                            <div class="form-group row count">
                                                <div class="col-xl-4 col-lg-3"><input class="form-control form-control-lg form-control-solid" type="text" name="values[{{$loop->iteration-1}}][key]"  placeholder="Enter key" value="{{$setting->key}}"></div>
                                                <div class="col-xl-8" style="display: flex"><input type="text" class="form-control form-control-lg form-control-solid" name="values[{{$loop->iteration-1}}][value]" placeholder="Enter Value" value="{{$setting->value}}"/><a href="javascript:void(0)" class="trash-button btn btn-sm btn-default  btn-hover-danger btn-icon mt-2">
                                                        <i class="la la-trash" style="font-size: 30px;color: red" title="delete"></i></a></div>
                                            </div>
                                        @endforeach
                                       @endif

                                </div>

                                <!--end::Body-->
                            </form>
                            <!--end::Form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        var KTFormControls = function () {

            // Private functions
            const loginButton = document.getElementById('saveButton');
            form =  document.getElementById('ProfileForm');
            var _initDemo1 = function () {
                const fv =  FormValidation.formValidation(
                    document.getElementById('ProfileForm'),
                    {

                        plugins: { //Learn more: https://formvalidation.io/guide/plugins
                            trigger: new FormValidation.plugins.Trigger(),
                            // Bootstrap Framework Integration
                            bootstrap: new FormValidation.plugins.Bootstrap(),
                            // Validate fields when clicking the Submit button
                            submitButton: new FormValidation.plugins.SubmitButton(),
                            // Submit the form when all fields are valid
                            defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        }
                    }
                );
                loginButton.addEventListener('click', function() {
                    fv.validate().then(function(status) {
                    });
                });
            }
            return {
                // public functions
                init: function() {
                    _initDemo1();

                }
            };
        }();

        jQuery(document).ready(function() {
            KTFormControls.init();
            $('.add-more-button').click(function () {
             var length=$('.count').length;
             var length=length-1;
               var dummy=$('.admin-settings').clone();
               $(dummy).removeClass('admin-settings');
               $(dummy).find('#key').attr('name','values['+length+'][key]');
               $(dummy).find('#value').attr('name','values['+length+'][value]');
                $(dummy).show();
               $('.settingContainer').append(dummy);

            });
            $('.trash-button').click(function () {
                $(this).parent().parent().remove();
            })
        });
    </script>
@endsection
