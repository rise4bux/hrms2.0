@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5"> User Profile</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="{{ route('view.profile') }}" class="text-muted">Profile</a>
                            </li>

                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Daterange-->
                    <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="" data-toggle="tooltip" title="" data-placement="left">
                        <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                        <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                    </a>
                    <!--end::Daterange-->
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <div class="d-flex flex-row">
                <!--begin::Aside-->

                 @include('backend.snippets.user_profile')
                <!--end::Aside-->
                <!--begin::Content-->
                <div class="flex-row-fluid ml-lg-8">
                    <!--begin::Card-->
                    <div class="card card-custom card-stretch">
                        <!--begin::Header-->
                        <div class="card-header py-3">
                            <div class="card-title align-items-start flex-column">
                                <h3 class="card-label font-weight-bolder text-dark">Personal Information</h3>
                                <span class="text-muted font-weight-bold font-size-sm mt-1">Update your personal informaiton</span>
                            </div>
                            <div class="card-toolbar">
                                <button type="button" class="btn btn-success mr-2" id="saveButton">Save Changes</button>
                                <button type="reset" class="btn btn-secondary" onclick="return window.location.href='{{ route('dashboard') }}'">Cancel</button>
                            </div>
                        </div>
                        <!--end::Header-->
                        <!--begin::Form-->
                        <form id="ProfileForm" class="form"
                              method="POST" enctype="multipart/form-data"
                              action="{{ route('update.profile', $user->id) }}">
                        @csrf
                            <!--begin::Body-->
                            <div class="card-body">
                                <div class="row">
                                    <label class="col-xl-3"></label>
                                    <div class="col-lg-9 col-xl-6">
                                        {{--<h5 class="font-weight-bold mb-6">Company Info</h5>--}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label text-right">Profile Logo</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <div class="image-input image-input-outline" id="kt_profile_avatar" style="background-image: url({{ asset('theme/media/users/blank.png') }})">
                                            @if(isset($user) && $user->logo != '' )
                                                <div class="image-input-wrapper" style="background-image: url({{ asset('profile-image/' . $user->logo) }})"></div>
                                            @else
                                                <div class="image-input-wrapper" style="background-image: url({{ asset('theme/media/users/blank.png') }})"></div>
                                            @endif
                                            <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                                <i class="fa fa-pen icon-sm text-muted"></i>
                                                <input type="file" name="profile_avatar" accept=".png, .jpg, .jpeg" />

                                                    <input type="hidden" name="profile_avatar_remove" />

                                            </label>
                                            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
																	<i class="ki ki-bold-close icon-xs text-muted"></i>
																</span>
                                                                <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="Remove avatar" style="{!! (isset($user) && $user->logo == '') ? 'display:none':'display:block' !!}">
																	<i class="ki ki-bold-close icon-xs text-muted"></i>
																</span>
                                        </div>
                                        <span class="form-text text-muted">Allowed file types: png, jpg, jpeg.</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label text-right">Name</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <input class="form-control form-control-lg form-control-solid" name="name" type="text" value="{!! isset($user) ? $user->name : old('name') !!}"/>
                                    </div>
                                </div>
{{--                                <div class="form-group row">--}}
{{--                                    <label class="col-xl-3 col-lg-3 col-form-label text-right">Last Name</label>--}}
{{--                                    <div class="col-lg-9 col-xl-6">--}}
{{--                                        <input class="form-control form-control-lg form-control-solid" type="text"  name="last_name" value="{!! isset($user) ? $user->last_name : old('last_name') !!}" />--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <div class="row">
                                    <label class="col-xl-3"></label>
                                    <div class="col-lg-9 col-xl-6">
                                        <h5 class="font-weight-bold mt-10 mb-6">Contact Info</h5>
                                    </div>
                                </div>
{{--                                <div class="form-group row">--}}
{{--                                    <label class="col-xl-3 col-lg-3 col-form-label text-right">Contact Phone</label>--}}
{{--                                    <div class="col-lg-9 col-xl-6">--}}
{{--                                        <div class="input-group input-group-lg input-group-solid">--}}
{{--                                            <div class="input-group-prepend">--}}
{{--																	<span class="input-group-text">--}}
{{--																		<i class="la la-phone"></i>--}}
{{--																	</span>--}}
{{--                                            </div>--}}
{{--                                            <input type="text" class="form-control form-control-lg form-control-solid"  name="phone" value="{!! isset($user) ? $user->phone : old('phone') !!}" placeholder="Phone" />--}}
{{--                                        </div>--}}
{{--                                        <span class="form-text text-muted">We'll never share your email with anyone else.</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label text-right">Email Address</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <div class="input-group input-group-lg input-group-solid">
                                            <div class="input-group-prepend">
																	<span class="input-group-text">
																		<i class="la la-at"></i>
																	</span>
                                            </div>
                                            <input type="text"  class="form-control form-control-lg form-control-solid" name="email" value="{!! isset($user) ? $user->email : old('email') !!}" placeholder="Email" />
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--end::Body-->
                        </form>
                        <!--end::Form-->
                    </div>
                </div>

                <!--end::Content-->
            </div>

        </div>
        <!--end::Content-->
    </div>


@endsection

@section('scripts')
    <script src="{{ asset('theme/js/pages/custom/profile/profile.js?v=7.0.3') }}"></script>
    <script>

        var KTFormControls = function () {
            // Private functions
            const loginButton = document.getElementById('saveButton');

            var _initDemo1 = function () {
               const fv =  FormValidation.formValidation(
                    document.getElementById('ProfileForm'),
                    {
                        fields: {
                            name: {
                                validators: {
                                    notEmpty: {
                                        message: 'Name is required'
                                    },

                                }
                            },
                            email: {
                                validators: {
                                    notEmpty: {
                                        message: 'Email is required'
                                    },
                                    emailAddress: {
                                        message: 'The value is not a valid email address'
                                    }
                                }
                            },
                            profile_avatar: {
                                validators: {

                                    file: {
                                        extension: 'jpeg,jpg,png',
                                        type: 'image/jpeg,image/png',
                                        maxSize: 2097152,   // 2048 * 1024
                                        message: 'The selected file is not valid'
                                    },
                                }
                            },

                           /* confirm_password: {
                                validators: {

                                    identical: {
                                        compare: function() {
                                            return form.querySelector('[name="password"]').value;
                                        },
                                        message: 'The password and its confirm are not the same'
                                    }

                                }
                            }*/
                        },

                        plugins: { //Learn more: https://formvalidation.io/guide/plugins
                            trigger: new FormValidation.plugins.Trigger(),
                            // Bootstrap Framework Integration
                            bootstrap: new FormValidation.plugins.Bootstrap(),
                            // Validate fields when clicking the Submit button
                            submitButton: new FormValidation.plugins.SubmitButton(),
                            // Submit the form when all fields are valid
                            defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        }
                    }
                );
                loginButton.addEventListener('click', function() {
                    fv.validate().then(function(status) {
                        // Update the login button content based on the validation status
                       /* if (status == 'Valid') {

                        }
                        else {

                        }*/
                    });
                });
            }
            return {
                // public functions
                init: function() {
                    _initDemo1();

                }
            };
        }();

        jQuery(document).ready(function() {
            KTFormControls.init();
        });
    </script>
@endsection



