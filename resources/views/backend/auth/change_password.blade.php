@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5"> User Profile</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="{{ route('view.profile') }}" class="text-muted">Profile</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('change.password') }}" class="text-muted"> Change Password </a>
                            </li>

                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Daterange-->
                    <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="" data-toggle="tooltip" title="" data-placement="left">
                        <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                        <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                    </a>
                    <!--end::Daterange-->
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <div class="d-flex flex-row">
                <!--begin::Aside-->

            @include('backend.snippets.user_profile')
            <!--end::Aside-->
                <!--begin::Content-->
                <div class="flex-row-fluid ml-lg-8">
                    <!--begin::Content-->
                    <div class="flex-row-fluid ml-lg-8">
                        <!--begin::Card-->
                        <div class="card card-custom card-stretch">
                            <!--begin::Header-->
                            <div class="card-header py-3">
                                <div class="card-title align-items-start flex-column">
                                    <h3 class="card-label font-weight-bolder text-dark">Change Password</h3>
                                    <span class="text-muted font-weight-bold font-size-sm mt-1">Change your account password</span>
                                </div>
                                <div class="card-toolbar">
                                    <button type="button" class="btn btn-success mr-2" id="saveButton">Save Changes</button>
                                    <button type="reset" class="btn btn-secondary" onclick="return window.location.href='{{ route('view.profile') }}'">Cancel</button>
                                </div>
                            </div>
                            <!--end::Header-->
                            <!--begin::Form-->
                            <form id="ProfileForm" class="form"
                                  method="POST" enctype="multipart/form-data"
                                  action="{{ route('update.password', $user->id) }}">
                            @csrf
                            <!--begin::Body-->
                                <div class="card-body">
                                    <!--begin::Alert-->

                                    <!--end::Alert-->
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label text-alert">Current Password</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="password" class="form-control form-control-lg form-control-solid mb-2" value="" id="current_password" name="current_password" placeholder="Current password" />
{{--                                            <a href="{{route('password.request')}}" class="text-sm font-weight-bold">Forgot password ?</a>--}}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label text-alert">New Password</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="password" class="form-control form-control-lg form-control-solid" value="" name="new_password" placeholder="New password" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label text-alert">Verify Password</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="password" class="form-control form-control-lg form-control-solid" value="" name="confirm_password" placeholder="Verify password" />
                                        </div>
                                    </div>
                                </div>
                                <!--end::Body-->
                            </form>
                            <!--end::Form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>

        var KTFormControls = function () {
            // Private functions
            const loginButton = document.getElementById('saveButton');
            form =  document.getElementById('ProfileForm');
            var _initDemo1 = function () {
                const fv =  FormValidation.formValidation(
                    document.getElementById('ProfileForm'),
                    {
                        fields: {
                            current_password: {
                                validators: {
                                    notEmpty: {
                                        message: 'Current Password is required'
                                    },
                                    stringLength: {
                                        min: 8,
                                        message: 'The Password must be less 8 character long'
                                    }

                                }
                            },
                            new_password: {
						    validators: {
                                notEmpty: {
                                    message: 'Password is required'
                                },
                                stringLength: {
                                    min: 8,
                                    message: 'The Password must be less 8 character long'
                                }

                            }
					        },
                            confirm_password: {
                                    validators: {
                                        notEmpty: {
                                            message: 'Confirm Password is required'
                                        },
                                        identical: {
                                            compare: function() {
                                                return form.querySelector('[name="new_password"]').value;
                                            },
                                            message: 'The password and its confirm are not the same'
                                        }

                                    }
                                }
                        },

                        plugins: { //Learn more: https://formvalidation.io/guide/plugins
                            trigger: new FormValidation.plugins.Trigger(),
                            // Bootstrap Framework Integration
                            bootstrap: new FormValidation.plugins.Bootstrap(),
                            // Validate fields when clicking the Submit button
                            submitButton: new FormValidation.plugins.SubmitButton(),
                            // Submit the form when all fields are valid
                            defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        }
                    }
                );
                loginButton.addEventListener('click', function() {
                    fv.validate().then(function(status) {
                        // Update the login button content based on the validation status
                        /* if (status == 'Valid') {

                         }
                         else {

                         }*/
                    });
                });
            }
            return {
                // public functions
                init: function() {
                    _initDemo1();

                }
            };
        }();

        jQuery(document).ready(function() {
            KTFormControls.init();
        });
    </script>
@endsection
