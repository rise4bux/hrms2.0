@extends('backend.layouts.auth_master')
@section('content')

    <div class="login login-4 wizard d-flex flex-column flex-lg-row flex-column-fluid">
        <!--begin::Content-->
        <div class="login-container order-2 order-lg-1 d-flex flex-center flex-row-fluid px-7 pt-lg-0 pb-lg-0 pt-4 pb-6 bg-white">
            <!--begin::Wrapper-->
            <!--begin::Signin-->
            <div class="login-content d-flex flex-column pt-lg-0 pt-12">
                <!--begin::Logo-->
                <a href="{{ route('login') }}" class="login-logo pb-xl-20 pb-15">
                    {{--<img src="{{ asset('theme/media/logos/logo-4.png') }}" class="max-h-70px" alt=""/>--}}
                    <img src="{{ asset('images/site_logo.png') }}" class="max-h-150px" alt="">
                </a>
                <div class="login-form">
                    <!--begin::Form-->
                    <form class="form" id="kt_login_singin_form" action="{{ route('login') }}" method="post">
                    @csrf
                    <!--begin::Title-->
                        <div class="pb-5 pb-lg-15">
                            <h3 class="font-weight-bolder text-primary font-size-h2 font-size-h1-lg">Sign In</h3>
{{--                            <div class="text-muted font-weight-bold font-size-h4">--}}
{{--                                New Here?--}}
{{--                                <a href="{{ route('register') }}" class="text-primary font-weight-bolder">Create Account</a>--}}
{{--                            </div>--}}
                        </div>
                        <!--begin::Title-->

                        <!--begin::Form group-->
                        <div class="form-group">
                            <label class="font-size-h6 font-weight-bolder text-dark">Your Email</label>
                            <input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg border-0" type="text" name="email" autocomplete="off" value="{{ old('email') }}"/>
                        </div>
                        <!--end::Form group-->

                        <!--begin::Form group-->
                        <div class="form-group">
                            <div class="d-flex justify-content-between mt-n5">
                                <label class="font-size-h6 font-weight-bolder text-dark pt-5">Password</label>

                                {{-- <a href="{{ route('password.request') }}" class="text-primary font-size-h6 font-weight-bolder text-hover-primary pt-5">
                                     Forgot Password ?
                                 </a>--}}
                            </div>
                            <input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg border-0" type="password" name="password" autocomplete="off"/>
                        </div>
                        <!--end::Form group-->

                        <!--begin::Action-->
                        <div class="pb-lg-0 pb-5">
                            <button type="submit" id="kt_login_singin_form_submit_button" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3">Sign In</button>
                            <a href="{{ route('password.request') }}" class="text-primary font-weight-bolder">Forgot Password ?</a>
                        </div>
                        <!--end::Action-->
                    </form>
                    <!--end::Form-->
                </div>
            </div>
            <!--end::Signin-->
            <!--end::Wrapper-->
        </div>
        <!--begin::Content-->

        <!--begin::Aside-->
        <div class="login-aside order-1 order-lg-2 bgi-no-repeat bgi-position-x-right">
            <div class="login-conteiner bgi-no-repeat bgi-position-x-right bgi-position-y-bottom" style="background-image: url({{--{{ asset('theme/media/svg/illustrations/login-visual-4.svg') }}--}});">
                <!--begin::Aside title-->
                <h3 class="pt-lg-40 pl-lg-20 pb-lg-0 pl-10 py-20 m-0 d-flex justify-content-lg-start font-weight-boldest display5 display1-lg text-white">
                    User Roles Admin Portal
                </h3>
                <!--end::Aside title-->
            </div>
        </div>
        <!--end::Aside-->
    </div>



@endsection

@section('scripts')
    <script>
      /*  $(document).ready(function () {
            $.validator.setDefaults({
                debug: true,
                success: "valid"
            });
            $('#form').validate({
                rules: {
                    email: {
                        email: true,
                        required: true
                    },
                    password: {
                        required: true,
                        minlength: 6
                    }
                },messages: {
                    email: {
                        required: "Email field is required",
                        email: "Please use valid email format"
                    },
                    password: {
                        required: "Password field is required",
                        minlength: "Please enter at least 6 characters."
                    }
                },

                submitHandler: function (form) {
                    form.submit();
                }
            });
        });*/
    </script>
@endsection
