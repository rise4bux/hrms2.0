@section('title','Role')
@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5">Add Role</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="{{ route('role.index') }}" class="text-muted">View</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Daterange-->
                    <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="" data-toggle="tooltip" title="" data-placement="left">
                        <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                        <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                    </a>
                    <!--end::Daterange-->
                    <!--begin::Dropdown-->
                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>

        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <div class="row">
                <div class="col-lg-12">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">Add Role</h3>
                        </div>
                        <!--begin::Form-->
                        <form id="companyForm" class="form m-form m-form--fit m-form--label-align-right"
                              action="{{ isset($roleData) && !empty($roleData) ? route('role.store') .'/'. $roleData->id : route('role.store') }}"
                              method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body row">
                                <div class="col-6">
                                    <div class="form-group col-lg-12">
                                        <label class="form-control-label">Name</label>
                                        <input type="text" class="form-control" name="name" value="{{ isset($roleData) && !empty($roleData) ? $roleData->name : '' }} "/>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button type="submit" class="btn btn-success font-weight-bold mr-2">Add</button>
                                </div>
                            </div>
                            <!--end::Code example-->
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <!--end::Card-->
                </div>
            </div>
        </div>
        <!--end::Content-->
    </div>
@endsection

@section('scripts')
    <script>
        var KTFormControls = function () {
            // Private functions
            var _initDemo1 = function () {
                $('#group_id').select2({placeholder: "Select Group"});
                // FormValidation.formValidation(
                //         document.getElementById('companyForm'),
                //         {
                //             fields: {
                //                 first_name: {
                //                     validators: {
                //                         notEmpty: {
                //                             message: 'First Name is required'
                //                         },
                //
                //                     }
                //                 },
                //                 last_name: {
                //                     validators: {
                //                         notEmpty: {
                //                             message: 'Last Name is required'
                //                         },
                //
                //                     }
                //                 },
                //                 email: {
                //                     validators: {
                //                         notEmpty: {
                //                             message: 'Email is required'
                //                         },
                //                         emailAddress: {
                //                             message: 'The value is not a valid email address'
                //                         }
                //                     }
                //                 },
                //             },
                //
                //             plugins: {//Learn more: https://formvalidation.io/guide/plugins
                //                 trigger: new FormValidation.plugins.Trigger(),
                //                 // Bootstrap Framework Integration
                //                 bootstrap: new FormValidation.plugins.Bootstrap(),
                //                 // Validate fields when clicking the Submit button
                //                 submitButton: new FormValidation.plugins.SubmitButton(),
                //                 // Submit the form when all fields are valid
                //                 defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                //             }
                //         }
                // );
            }
            return {
                // public functions
                init: function () {
                    _initDemo1();

                }
            };
        }();

        jQuery(document).ready(function () {
            KTFormControls.init();
        });


    </script>
@endsection
