@section('title','Module Master')
@extends('backend.layouts.master')
@section('content')

    <!--begin::Content Wrapper-->
    <div class="main d-flex flex-column flex-row-fluid">

        <div class="content flex-column-fluid" id="kt_content">
            <div class="row">
                <div class="col-md-12">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">Create Module</h3>
                        </div>
                        <!--begin::Form-->

                        <form action="{{ isset($moduleMasterData) && !empty($moduleMasterData) ? route("modulemaster.store").'/'.$moduleMasterData->id : route("modulemaster.store") }}" method="POST">
                            @csrf
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="module_name" class="col-2 col-form-label">Name</label>
                                    <div class="col-10">
                                        <input class="form-control" type="text" id="name" name="moduleName" value="{{ isset($moduleMasterData) && !empty($moduleMasterData) ? $moduleMasterData->name : '' }}" >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="url" class="col-2 col-form-label">URL</label>
                                    <div class="col-10">
                                        <input class="form-control" type="text" name="url" value="{{ isset($moduleMasterData) && !empty($moduleMasterData) ? $moduleMasterData->module_url : '' }}" >
                                    </div>
                                </div>
                                @if(!isset($id))
                                    @livewire('get-sub-parent',['parent_id'=>0])
                                @endif
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-2"></div>
                                    <div class="col-10">
                                        <button type="submit" class="btn btn-success mr-2">Submit</button>
                                        <button type="reset" class="btn btn-secondary">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Card-->
                </div>
            </div>
        </div>
        <!--end::Content-->
    </div>
    <!--begin::Content Wrapper-->

@endsection
@section('scripts')
    <script>

    </script>
@endsection
