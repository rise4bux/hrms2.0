@section('title','Module Master ')
@extends('backend.layouts.master')
@section('content')
    <!--begin::Content Wrapper-->
    <div class="main d-flex flex-column flex-row-fluid">
        <div class="content flex-column-fluid" id="kt_content">
            <div class="row">
                <div class="col-md-12">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">View</h3>
                            <div class="py-4">
                                <a href="{{ route('modulemaster.create') }}" class="btn btn-light-primary font-weight-bold btn-sm px-2 font-size-base ml-2">Add Module</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin: Datatable-->
                            <table class="table table-bordered table-checkable" >
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Module Controller</th>
                                    <th>Parent Id</th>
                                    <th>Module URL</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($moduleMasterData as $row)
                                    <tr>
                                        <td>{{ $row->id }}</td>
                                        <td>{{ ucfirst($row->name) }}</td>
                                        <td>{{ $row->module_controller ? $row->module_controller : 'N/A' }}</td>
                                        <td>{{ $row->parent_id }}</td>
                                        <td>{{ $row->module_url ? $row->module_url : 'N/A' }}</td>
                                        <td>
                                            <a href="{{ route('modulemaster.create',\Illuminate\Support\Facades\Crypt::encrypt($row->id)) }}" class="btn btn-sm"><i class=" text-primary fa fa-edit"></i></a>
                                            <a href="{{ route('modulemaster.delete',\Illuminate\Support\Facades\Crypt::encrypt($row->id)) }}" class="btn btn-sm p-0"><i class=" text-danger fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <!--end: Datatable-->
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-12">
                                    <button class="btn btn-success mr-2" id="add" >Add</button>
                                    <button class="btn btn-secondary">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Card-->
                </div>
            </div>
        </div>
        <!--end::Content-->
    </div>
    <!--begin::Content Wrapper-->
@endsection
@section('scripts')

@endsection
