@extends('backend.layouts.master')
@section('content')
<div class="main d-flex flex-column flex-row-fluid">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4" id="kt_subheader">
        <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-2 mr-5"> {!! isset($items) ? 'Update Item' : 'Add Item' !!}</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="{{ route('webshop-item.index') }}" class="text-muted">Items</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{{ isset($items) ? route('webshop-item.index').'/'.$items->id.'/edit' : route('webshop-item.create') }} " class="text-muted">{!! isset($items) ? 'Update Item' : 'Add Item' !!}</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <!--begin::Daterange-->
                <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="" data-toggle="tooltip" title="" data-placement="left">
                    <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                    <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                </a>
                <!--end::Daterange-->
                <!--begin::Dropdown-->

                <!--end::Dropdown-->
            </div>

            <!--end::Toolbar-->
        </div>

    </div>

    <!--end::Subheader-->
    <div class="content flex-column-fluid" id="kt_content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Card-->
                <div class="card card-custom gutter-b example example-compact">
                    <div class="card-header">
                        <h3 class="card-title">{!! isset($items) ? 'Update Item' : 'Add Item' !!} </h3>
                    </div>
                    <!--begin::Form-->
                    <form id="companyForm" class="form m-form m-form--fit m-form--label-align-right"
                          action="{{ isset($items) ? route('webshop-item.update', $items->id) : route('webshop-item.store')}}"
                          method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body row">
                            <div class="col-12 text-right my-auto">
                                <button type="submit" class="btn btn-success font-weight-bold mr-2">Update</button>
                                <button type="button" class="btn btn-light-success font-weight-bold" onclick="return window.location.href ='{{ route('webshop-item.index') }}'">Cancel</button>
                            </div>
                            <div class="col-6">
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Description</label>
                                    <input type="text" class="form-control" disabled  name="Description" id="Description" value="{!! isset($items) ? $items->Description: old('Description') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Description 2</label>
                                    <input type="text" class="form-control" disabled  name="Description_2" id="Description_2" value="{!! isset($items) ? $items->Description_2: old('Description_2') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Width</label>
                                    <input type="text" class="form-control" disabled  name="Width" id="Width" value="{!! isset($items) ? $items->Width: old('Width') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Aspect Ratio</label>
                                    <input type="text" class="form-control" disabled  name="Aspect_Ratio" id="Aspect_Ratio" value="{!! isset($items) ? $items->Aspect_Ratio: old('Aspect_Ratio') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Load Range</label>
                                    <input type="text" class="form-control" disabled  name="Load_Range" id="Load_Range" value="{!! isset($items) ? $items->Load_Range: old('Load_Range') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Diameter</label>
                                    <input type="text" class="form-control" disabled  name="Diameter" id="Diameter" value="{!! isset($items) ? $items->Diameter: old('Diameter') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Loadindex 1 single tire</label>
                                    <input type="text" class="form-control" disabled  name="Loadindex_1_single_tire" id="Loadindex_1_single_tire" value="{!! isset($items) ? $items->Loadindex_1_single_tire: old('Loadindex_1_single_tire') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Loadindex 1 twin tire</label>
                                    <input type="text" class="form-control" disabled  name="Loadindex_1_twin_tire" id="Loadindex_1_twin_tire" value="{!! isset($items) ? $items->Loadindex_1_twin_tire: old('Loadindex_1_twin_tire') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Speed index</label>
                                    <input type="text" class="form-control" disabled  name="Speedindex" id="Speedindex" value="{!! isset($items) ? $items->Speedindex: old('Speedindex') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Speedindex 2</label>
                                    <input type="text" class="form-control" disabled  name="Speedindex_2" id="Speedindex_2" value="{!! isset($items) ? $items->Speedindex_2: old('Speedindex_2') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Addon Information</label>
                                    <input type="text" class="form-control" disabled  name="Addon_Information" id="Addon_Information" value="{!! isset($items) ? $items->Addon_Information: old('Addon_Information') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Manufacturer Code</label>
                                    <input type="text" class="form-control" disabled  name="Manufacturer_Code" id="Manufacturer_Code" value="{!! isset($items) ? $items->Manufacturer_Code: old('Manufacturer_Code') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Manufacturer Item No</label>
                                    <input type="text" class="form-control" disabled  name="Manufacturer_Item_No" id="Manufacturer_Item_No" value="{!! isset($items) ? $items->Manufacturer_Item_No: old('Manufacturer_Item_No') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Main Group Code</label>
                                    <input type="text" class="form-control" disabled  name="Main_Group_Code" id="Main_Group_Code" value="{!! isset($items) ? $items->Main_Group_Code: old('Main_Group_Code') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Sub Group Code</label>
                                    <input type="text" class="form-control" disabled  name="Sub_Group_Code" id="Sub_Group_Code" value="{!! isset($items) ? $items->Sub_Group_Code: old('Sub_Group_Code') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Position Group Code</label>
                                    <input type="text" class="form-control" disabled  name="Position_Group_Code" id="Position_Group_Code" value="{!! isset($items) ? $items->Position_Group_Code: old('Position_Group_Code') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Branch Item No</label>
                                    <input type="text" class="form-control" disabled  name="Branch_Item_No" id="Branch_Item_No" value="{!! isset($items) ? $items->Branch_Item_No: old('Branch_Item_No') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Noise Performance</label>
                                    <input type="text" class="form-control" disabled  name="Noise_Performance" id="Noise_Performance" value="{!! isset($items) ? $items->Noise_Performance: old('Noise_Performance') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Noise Class Type</label>
                                    <input type="text" class="form-control" disabled  name="Noise_Class_Type" id="Noise_Class_Type" value="{!! isset($items) ? $items->Noise_Class_Type: old('Noise_Class_Type') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Fuel Efficiency</label>
                                    <input type="text" class="form-control" disabled  name="Fuel_Efficiency" id="Fuel_Efficiency" value="{!! isset($items) ? $items->Fuel_Efficiency: old('Fuel_Efficiency') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Wet Grip</label>
                                    <input type="text" class="form-control" disabled  name="Wet_Grip" id="Wet_Grip" value="{!! isset($items) ? $items->Wet_Grip: old('Wet_Grip') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Last DateTime Modified</label>
                                    <input type="text" class="form-control" disabled  name="Last_DateTime_Modified" id="Last_DateTime_Modified" value="{!! isset($items) ? $items->Last_DateTime_Modified: old('Last_DateTime_Modified') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Inventory</label>
                                    <input type="text" class="form-control" disabled  name="Inventory" id="Inventory" value="{!! isset($items) ? $items->Inventory: old('Inventory') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">EAN No</label>
                                    <input type="text" class="form-control" disabled  name="EAN_No" id="EAN_No" value="{!! isset($items) ? $items->EAN_No: old('EAN_No') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Manufacturer Name</label>
                                    <input type="text" class="form-control" disabled  name="Manufacturer_Name" id="Manufacturer_Name" value="{!! isset($items) ? $items->Manufacturer_Name: old('Manufacturer_Name') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">URL_2</label>
                                    <input type="text" class="form-control" disabled  name="URL_2" id="URL_2" value="{!! isset($items) ? $items->URL_2: old('URL_2') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Quantity24</label>
                                    <input type="text" class="form-control" disabled  name="Quantity24" id="Quantity24" value="{!! isset($items) ? $items->Quantity24: old('Quantity24') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Quantity48</label>
                                    <input type="text" class="form-control" disabled  name="Quantity48" id="Quantity48" value="{!! isset($items) ? $items->Quantity48: old('Quantity48') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Auxiliary Index 1</label>
                                    <input type="text" class="form-control" disabled  name="AuxiliaryIndex1" id="AuxiliaryIndex1" value="{!! isset($items) ? $items->AuxiliaryIndex1: old('AuxiliaryIndex1') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Auxiliary Index 2</label>
                                    <input type="text" class="form-control" disabled  name="AuxiliaryIndex2" id="AuxiliaryIndex2" value="{!! isset($items) ? $items->AuxiliaryIndex2: old('AuxiliaryIndex2') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Auxiliary Index 3</label>
                                    <input type="text" class="form-control" disabled  name="AuxiliaryIndex3" id="AuxiliaryIndex3" value="{!! isset($items) ? $items->AuxiliaryIndex3: old('AuxiliaryIndex3') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Auxiliary Index 4</label>
                                    <input type="text" class="form-control" disabled  name="AuxiliaryIndex4" id="AuxiliaryIndex4" value="{!! isset($items) ? $items->AuxiliaryIndex4: old('AuxiliaryIndex4') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Auxiliary Index 5</label>
                                    <input type="text" class="form-control" disabled  name="AuxiliaryIndex5" id="AuxiliaryIndex5" value="{!! isset($items) ? $items->AuxiliaryIndex5: old('AuxiliaryIndex5') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Auxiliary Index 6</label>
                                    <input type="text" class="form-control" disabled  name="AuxiliaryIndex6" id="AuxiliaryIndex6" value="{!! isset($items) ? $items->AuxiliaryIndex6: old('AuxiliaryIndex6') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Auxiliary Index 7</label>
                                    <input type="text" class="form-control" disabled  name="AuxiliaryIndex7" id="AuxiliaryIndex7" value="{!! isset($items) ? $items->AuxiliaryIndex7: old('AuxiliaryIndex7') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Auxiliary Index 8</label>
                                    <input type="text" class="form-control" disabled  name="AuxiliaryIndex8" id="AuxiliaryIndex8" value="{!! isset($items) ? $items->AuxiliaryIndex8: old('AuxiliaryIndex8') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Auxiliary Index 9</label>
                                    <input type="text" class="form-control" disabled  name="AuxiliaryIndex9" id="AuxiliaryIndex9" value="{!! isset($items) ? $items->AuxiliaryIndex9: old('AuxiliaryIndex9') !!}"/>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Unit Price</label>
                                    <input type="text" class="form-control"  name="Unit_Price" id="Unit_Price" value="{!! isset($items) ? $items->Unit_Price: old('Unit_Price') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Price24</label>
                                    <input type="text" class="form-control"  name="Price24" id="Price24" value="{!! isset($items) ? $items->Price24: old('Price24') !!}"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="form-control-label">Price48</label>
                                    <input type="text" class="form-control"  name="Price48" id="Price48" value="{!! isset($items) ? $items->Price48: old('Price48') !!}"/>
                                </div>
                            </div>
                        </div>
                        <!--end::Code example-->
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Card-->
                <!--begin::Card-->

                <!--end::Card-->
            </div>

        </div>
    </div>
    <!--end::Content-->
</div>
@endsection

@section('scripts')
<script>
    var KTFormControls = function () {
        // Private functions
        var _initDemo1 = function () {
             $('#group_id').select2({placeholder: "Select Group"});
            // FormValidation.formValidation(
            //         document.getElementById('companyForm'),
            //         {
            //             fields: {
            //                 first_name: {
            //                     validators: {
            //                         notEmpty: {
            //                             message: 'First Name is required'
            //                         },
            //
            //                     }
            //                 },
            //                 last_name: {
            //                     validators: {
            //                         notEmpty: {
            //                             message: 'Last Name is required'
            //                         },
            //
            //                     }
            //                 },
            //                 email: {
            //                     validators: {
            //                         notEmpty: {
            //                             message: 'Email is required'
            //                         },
            //                         emailAddress: {
            //                             message: 'The value is not a valid email address'
            //                         }
            //                     }
            //                 },
            //             },
            //
            //             plugins: {//Learn more: https://formvalidation.io/guide/plugins
            //                 trigger: new FormValidation.plugins.Trigger(),
            //                 // Bootstrap Framework Integration
            //                 bootstrap: new FormValidation.plugins.Bootstrap(),
            //                 // Validate fields when clicking the Submit button
            //                 submitButton: new FormValidation.plugins.SubmitButton(),
            //                 // Submit the form when all fields are valid
            //                 defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            //             }
            //         }
            // );
        }
        return {
            // public functions
            init: function () {
                _initDemo1();

            }
        };
    }();

    jQuery(document).ready(function () {
        KTFormControls.init();
    });


</script>
@endsection
