@section('title', (isset($id) && !empty($id)) ? 'Update Department': 'Add Department' )
@extends('backend.layouts.master')
@section('content')
    <!--begin::Content Wrapper-->
    <div class="main d-flex flex-column flex-row-fluid">
        <div class="content flex-column-fluid" id="kt_content">
            <div class="row">
                <div class="col-md-12">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">{{ isset($departmentData) && !empty($departmentData) ? 'Update Department' : 'Add Department' }}</h3>
                        </div>
                        <!--begin::Form-->
                        <form action="{{ isset($id) && !empty($id) ? route("department.update",$id ) : route("department.store") }}" method="POST" autocomplete="off">
                            @csrf
                            @if(isset($id) && !empty($id))
                                @method('PUT')
                            @endif
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="name" class="col-2 col-form-label">Name</label>
                                    <div class="col-10">
                                        <input class="form-control" type="text" id="name" name="name" value="{{ (isset($departmentData) && !empty($departmentData)) ? $departmentData->name : '' }}">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-12">
                                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Card-->
                </div>
            </div>
        </div>
        <!--end::Content-->
    </div>
    <!--begin::Content Wrapper-->
@endsection
