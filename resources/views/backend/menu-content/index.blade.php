
@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">

        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <!--begin::Card-->
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Content Management
                        </h3>
                    </div>
                    <div class="col-12">
                        <ul class="nav nav-tabs nav-tabs-line nav-tabs-info">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#home">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#velgen">Velgen</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home">
                            <form class="form-input" action="menu-content" method="POST">
                                @csrf
                                <input type="hidden" name="menu_id" value="1">
                                <div class="row d-flex justify-content-around">
                                    <div class="col-12 mb-5 text-right">
                                        <button type="submit" class="btn btn-primary font-weight-bolder">
                                            <i class="icon-2x text-white flaticon-refresh"></i>
                                            Update
                                        </button>
                                    </div>
                                    <div class="col-11 p-3 mb-5 section-shadow">
                                        <label class="label-control">Content Top</label>
                                        <input type="hidden" name="section[1][name]" value="content-top">
                                        <textarea class="summernote" name="section[1][html]">
                                        @php
                                            // 1 refers to home menu
                                             if($menuContentData->has('1')){
                                                  $data = $menuContentData[1]->where('section_sequence',1)->first();
                                                  echo (isset($data) && !empty($data)) ? $data['content'] : '';
                                             }
                                        @endphp
                                        </textarea>
                                    </div>
                                    <div class="col-5 p-3 mb-5 section-shadow">
                                        <label class="label-control">Content Middle Left</label>
                                        <input type="hidden" name="section[2][name]" value="content-middle-left">
                                        <textarea class="summernote" name="section[2][html]">
                                            @php
                                                // 1 refers to home menu
                                                 if($menuContentData->has('1')){
                                                      $data = $menuContentData[1]->where('section_sequence',2)->first();
                                                      echo (isset($data) && !empty($data)) ? $data['content'] : '';
                                                 }
                                            @endphp
                                        </textarea>
                                    </div>
                                    <div class="col-5 p-3 mb-5 section-shadow">
                                        <label class="label-control">Content Middle Right</label>
                                        <input type="hidden" name="section[3][name]" value="content-middle-right">
                                        <textarea class="summernote" name="section[3][html]">
                                            @php
                                                // 1 refers to home menu
                                                 if($menuContentData->has('1')){
                                                      $data = $menuContentData[1]->where('section_sequence',3)->first();
                                                      echo (isset($data) && !empty($data)) ? $data['content'] : '';
                                                 }
                                            @endphp
                                        </textarea>
                                    </div>
                                    <div class="col-3 p-3 mb-5 section-shadow">
                                        <label class="label-control">Content Bottom Left</label>
                                        <input type="hidden" name="section[4][name]" value="content-bottom-left">
                                        <textarea class="summernote" name="section[4][html]">
                                            @php
                                                // 1 refers to home menu
                                                 if($menuContentData->has('1')){
                                                      $data = $menuContentData[1]->where('section_sequence',4)->first();
                                                      echo (isset($data) && !empty($data)) ? $data['content'] : '';
                                                 }
                                            @endphp
                                        </textarea>
                                    </div>
                                    <div class="col-3 p-3 mb-5 section-shadow">
                                        <label class="label-control">Content Bottom Center</label>
                                        <input type="hidden" name="section[5][name]" value="content-bottom-center">
                                        <textarea class="summernote" name="section[5][html]">
                                            @php
                                                // 1 refers to home menu
                                                 if($menuContentData->has('1')){
                                                      $data = $menuContentData[1]->where('section_sequence',5)->first();
                                                      echo (isset($data) && !empty($data)) ? $data['content'] : '';
                                                 }
                                            @endphp
                                        </textarea>
                                    </div>
                                    <div class="col-3 p-3 mb-5 section-shadow">
                                        <label class="label-control">Content Bottom Right</label>
                                        <input type="hidden" name="section[6][name]" value="content-bottom-right">
                                        <textarea class="summernote" name="section[6][html]">
                                            @php
                                                  // 1 refers to home menu
                                                    if($menuContentData->has('1')){
                                                         $data = $menuContentData[1]->where('section_sequence',6)->first();
                                                         echo (isset($data) && !empty($data)) ? $data['content'] : '';
                                                    }
                                            @endphp
                                        </textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="velgen" role="tabpanel" aria-labelledby="velgen">
                            <form action="menu-content" class="form-input" method="POST">
                                @csrf
                                <input type="hidden" name="menu_id" value="2">
                                <div class="row d-flex justify-content-around">
                                    <div class="col-12 mb-5 text-right">
                                        <button type="submit" class="btn btn-primary font-weight-bolder">
                                            <i class="icon-2x text-white flaticon-refresh"></i>
                                            Update
                                        </button>
                                    </div>
                                    <div class="col-11 p-3 mb-5 section-shadow">
                                        <label class="label-control">Content Top</label>
                                        <input type="hidden" name="section[1][name]" value="content-top">
                                        <textarea class="summernote shadow" name="section[1][html]">
                                            @php
                                              // 2 refers to velgen menu
                                                if($menuContentData->has('2')){
                                                     $data = $menuContentData[2]->where('section_sequence',1)->first();
                                                     echo (isset($data) && !empty($data)) ? $data['content'] : '';
                                                }
                                            @endphp
                                        </textarea>
                                    </div>
                                    <div class="col-3 p-3 mb-5 section-shadow">
                                        <label class="label-control">Content Middle Left</label>
                                        <input type="hidden" name="section[2][name]" value="content-middle-left">
                                        <textarea class="summernote" name="section[2][html]">
                                              @php
                                                  // 2 refers to velgen menu
                                                    if($menuContentData->has('2')){
                                                         $data = $menuContentData[2]->where('section_sequence',2)->first();
                                                         echo (isset($data) && !empty($data)) ? $data['content'] : '';
                                                    }
                                              @endphp
                                        </textarea>
                                    </div>
                                    <div class="col-3 p-3 mb-5 section-shadow">
                                        <label class="label-control">Content Middle Center</label>
                                        <input type="hidden" name="section[3][name]" value="content-middle-center">
                                        <textarea class="summernote" name="section[3][html]">
                                              @php
                                                  // 2 refers to velgen menu
                                                    if($menuContentData->has('2')){
                                                         $data = $menuContentData[2]->where('section_sequence',3)->first();
                                                         echo (isset($data) && !empty($data)) ? $data['content'] : '';
                                                    }
                                              @endphp
                                        </textarea>
                                    </div>
                                    <div class="col-3 p-3 mb-5 section-shadow">
                                        <label class="label-control">Content Middle Right</label>
                                        <input type="hidden" name="section[4][name]" value="content-middle-right">
                                        <textarea class="summernote" name="section[4][html]">
                                              @php
                                                  // 2 refers to velgen menu
                                                    if($menuContentData->has('2')){
                                                         $data = $menuContentData[2]->where('section_sequence',4)->first();
                                                         echo (isset($data) && !empty($data)) ? $data['content'] : '';
                                                    }
                                              @endphp
                                        </textarea>
                                    </div>
                                    <div class="col-11 p-3 mb-5 section-shadow">
                                        <label class="label-control">Content Bottom</label>
                                        <input type="hidden" name="section[5][name]" value="content-bottom">
                                        <textarea class="summernote" name="section[5][html]">
                                              @php
                                                  // 2 refers to velgen menu
                                                    if($menuContentData->has('2')){
                                                         $data = $menuContentData[2]->where('section_sequence',5)->first();
                                                         echo (isset($data) && !empty($data)) ? $data['content'] : '';
                                                    }
                                              @endphp
                                        </textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--end::Body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Content-->
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 200
            });
        });
    </script>
@endsection
